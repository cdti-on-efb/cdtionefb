/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genres;

import static junit.framework.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Harshit
 */
public class ETXTest {
    
    public ETXTest() {
    }

    /**
     * Test of getNb_words method, of class ETX.
     */
    @Test
    public void testGetNb_words() {
        System.out.println("getNb_words");
        ETX instance = new ETX();
        instance.setBits("100000110000011000010101");
        int expResult = 1557;
        instance.decryptValue();
        int result = instance.getNb_words();
        assertEquals(expResult,result);
        System.out.println("Nb words:" + result);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getParity method, of class ETX.
     */
    @Test
    public void testGetParity() {
        System.out.println("getParity");
        ETX instance = new ETX();
        instance.setBits("100000110000011000010101");
        int expResult = 1;
        instance.decryptValue();
        int result = instance.getParity();
        assertEquals(expResult, result);
        System.out.println("Parity:" + result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setParity method, of class ETX.
     */
    @Test
    public void testSetParity() {
        System.out.println("setParity");
        int parity = 1;
        ETX instance = new ETX();
        instance.setParity(parity);
        // TODO review the generated test code and remove the default call to fail.
        System.out.println(parity);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setNb_words method, of class ETX.
     */
    @Test
    public void testSetNb_words() {
        System.out.println("setNb_words");
        int nb_words = 1557;
        ETX instance = new ETX();
        instance.setNb_words(nb_words);
        System.out.println(nb_words);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of decryptValue method, of class ETX.
     */
    @Test
    public void testDecryptValue() {
        System.out.println("decryptValue");
        ETX instance = new ETX();
        instance.setBits("100000110000011000010101");
        instance.decryptValue();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
