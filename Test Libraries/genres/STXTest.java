/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genres;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Harshit
 */
public class STXTest {
    
    public STXTest() {
    }

    /**
     * Test of getParity method, of class STX.
     */
    @Test
    public void testGetParity() {
        System.out.println("getParity");
        STX instance = new STX();
        instance.setBits("100000101000011000100011");
        int expResult = 1;
        instance.decryptValue();
        int result = instance.getParity();
        assertEquals(expResult, result);
        System.out.println("Parity:" + result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setParity method, of class STX.
     */
    @Test
    public void testSetParity() {
        System.out.println("setParity:");
        int parity = 1;
        STX instance = new STX();
        instance.setParity(parity);
        System.out.println(parity);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getNb_words method, of class STX.
     */
    @Test
    public void testGetNb_words() {
        System.out.println("getNb_words");
        STX instance = new STX();
        instance.setBits("100000101000011000100011");
        int expResult = 1571;
        instance.decryptValue();
        int result = instance.getNb_words();
        assertEquals(expResult, result);
        System.out.println("Nb words:" + result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of setNb_words method, of class STX.
     */
    @Test
    public void testSetNb_words() {
        System.out.println("setNb_words");
        int nb_words = 0;
        STX instance = new STX();
        instance.setNb_words(nb_words);
        System.out.println(nb_words);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        
    }

    /**
     * Test of getAll_traffic_bit method, of class STX.
     */
    @Test
    public void testGetAll_traffic_bit() {
        System.out.println("getAll_traffic_bit");
        STX instance = new STX();
        String expResult = "Display Traffic";
        instance.setBits("100000101000011000100011");
        instance.decryptValue();
        String result = instance.getAll_traffic_bit();
        assertEquals(expResult, result);
        System.out.println("ALl Traffic: " + result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAll_traffic_bit method, of class STX.
     */
    @Test
    public void testSetAll_traffic_bit() {
        System.out.println("setAll_traffic_bit");
        String all_traffic = "";
        STX instance = new STX();
        instance.setAll_traffic_bit(all_traffic);
        System.out.println(all_traffic);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of decryptValue method, of class STX.
     */
    @Test
    public void testDecryptValue() {
        System.out.println("decryptValue");
        STX instance = new STX();
        instance.setBits("100000101000011000100011");
        instance.decryptValue();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
