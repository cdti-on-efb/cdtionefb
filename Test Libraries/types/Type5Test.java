/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Harshit
 */
public class Type5Test {
    
    
    public Type5Test() {
    }

    /**
     * Test of getPosition_offset method, of class Type5.
     */
    @Test
    public void testGetPosition_offset() {
        System.out.println("getPosition_offset");
        Type5 instance = new Type5();
        String expResult = "the position transmitted in the surface position message is not known to be referenced to the ADS-B Position Reference Point of the A/V. Thus, the aircraft depiction is typically extended beyond the length/width size.";
        instance.setBits("010000000000000000000101"); //1605
        instance.decryptValue();
        String result = instance.getPosition_offset();
        assertEquals(expResult, result);
        System.out.println("Position Offset:" + result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPosition_offset method, of class Type5.
     */
    @Test
    public void testSetPosition_offset() {
        System.out.println("setPosition_offset");
        String position_offset = "the position transmitted in the surface position message is not known to be referenced to the ADS-B Position Reference Point of the A/V. Thus, the aircraft depiction is typically extended beyond the length/width size.";
        Type5 instance = new Type5();
        instance.setPosition_offset(position_offset);
        System.out.println(position_offset);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getParity method, of class Type5.
     */
    @Test
    public void testGetParity() {
        System.out.println("getParity");
        Type5 instance = new Type5();
        instance.setBits("010000000000000000000101"); //1605
        instance.decryptValue();
        int expResult = 0;
        int result = instance.getParity();
        assertEquals(expResult, result);
        System.out.println("Parity:" + result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setParity method, of class Type5.
     */
    @Test
    public void testSetParity() {
        System.out.println("setParity");
        int parity = 0;
        Type5 instance = new Type5();
        instance.setParity(parity);
        System.out.println(parity);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getContinuation_bit method, of class Type5.
     */
    @Test
    public void testGetContinuation_bit() {
        System.out.println("getContinuation_bit");
        Type5 instance = new Type5();
        instance.setBits("010000000000000000000101");
        instance.decryptValue();
        int expResult = 1;
        int result = instance.getContinuation_bit();
        assertEquals(expResult, result);
        System.out.println("Continuation Bit:" + result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setContinuation_bit method, of class Type5.
     */
    @Test
    public void testSetContinuation_bit() {
        System.out.println("setContinuation_bit");
        int continuation_bit = 1;
        Type5 instance = new Type5();
        instance.setContinuation_bit(continuation_bit);
        System.out.println(continuation_bit);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getWidth_code method, of class Type5.
     */
    @Test
    public void testGetWidth_code() {
        System.out.println("getWidth_code");
        Type5 instance = new Type5();
        instance.setBits("010000000000000000000101"); //1605
        instance.decryptValue();
        int expResult = 0;
        int result = instance.getWidth_code();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setWidth_code method, of class Type5.
     */
    @Test
    public void testSetWidth_code() {
        System.out.println("setWidth_code");
        int width_code = 0;
        Type5 instance = new Type5();
        instance.setWidth_code(width_code);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getLength_code method, of class Type5.
     */
    @Test
    public void testGetLength_code() {
        System.out.println("getLength_code");
        Type5 instance = new Type5();
        int expResult = 000;
        instance.setBits("010000000000000000000101"); //1605
        instance.decryptValue();
        int result = instance.getLength_code();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setLength_code method, of class Type5.
     */
    @Test
    public void testSetLength_code() {
        System.out.println("setLength_code");
        int length_code = 0;
        Type5 instance = new Type5();
        instance.setLength_code(length_code);
        System.out.println(length_code);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getLen_wid_status method, of class Type5.
     */
    @Test
    public void testGetLen_wid_status() {
        System.out.println("getLen_wid_status");
        Type5 instance = new Type5();
        String expResult = "Invalid";
         instance.setBits("010000000000000000000101"); //1605
        instance.decryptValue();
        String result = instance.getLen_wid_status();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setLen_wid_status method, of class Type5.
     */
    @Test
    public void testSetLen_wid_status() {
        System.out.println("setLen_wid_status");
        String len_wid_status = "Invalid";
        Type5 instance = new Type5();
        instance.setLen_wid_status(len_wid_status);
        System.out.println(len_wid_status);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of decryptValue method, of class Type5.
     */
    @Test
    public void testDecryptValue() {
        System.out.println("decryptValue");
        Type5 instance = new Type5();
        instance.setBits("010000000000000000000101");
        instance.decryptValue();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
