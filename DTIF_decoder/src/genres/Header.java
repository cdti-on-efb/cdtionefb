package genres;

import main_package.*;

public class Header extends Value{
	
	private int nb_traffic_packets;
	private int nb_targets ;
	private int dtif_version;
	
	public Header() {super();}

	public Header(String value){
		super(value);
	}

	public int getParity() {
		return parity;
	}

	public void setParity(int parity) {
		this.parity = parity;
	}

	public int getDTIF_version() {
		return dtif_version;
	}

	public void setDTIF_version(int the_DTIF_version) {
		dtif_version = the_DTIF_version;
	}

	

	public int getNb_traffic_packets() {
		return nb_traffic_packets;
	}

	public void setNb_traffic_packets(int nb_traffic_packets) {
		this.nb_traffic_packets = nb_traffic_packets;
	}

	public int getNb_targets() {
		return nb_targets;
	}

	public void setNb_targets(int nb_targets) {
		this.nb_targets = nb_targets;
	}

	public void decryptValue() {
		
		// extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
		int the_parity = Character.getNumericValue(bits.charAt(0));
		
		// extracting the 7 bits containing number of  targets (bit number 24 to bit number 30 in arinc735b) in string format
		String the_nb_targets = bits.substring(2, 9); 
		// converting the string extracted containing the 7 bits to decimal value using the function parseInt already existing in java
		nb_targets = Integer.parseInt(the_nb_targets, 2);
		
		// extracting the 3 bits containing DTIF version number(bit number 16 to bit number 18 in arinc735b) in string format
		String the_DTIF_version = bits.substring(14, 17); 
		
		// converting the string extracted containing the 3 bits to decimal value using the function parseInt already existing in java
		int dtif_ver = Integer.parseInt(the_DTIF_version, 2);
		setDTIF_version(dtif_ver);
//		if(dtif_ver == 0) {
//			DTIF_version = "Version 0";
//		}else if(dtif_ver == 1) {
//			DTIF_version = "Version 1";
//		}else {
//			DTIF_version = "Invalid";
//		}
		
				 
		 		
		// extracting the 7 bits containing number of  Traffic Packets in DTIF file (bit number 9 to bit number 15 in arinc735b) in string format
		String the_nb_packets = bits.substring(17,24); 
		
		// converting the string extracted containing the 7 bits to decimal value using the function parseInt already existing in java
		nb_traffic_packets = Integer.parseInt(the_nb_packets, 2);
		
		/* setting every information extracted to the adequate attribute */
		setParity(the_parity); 
		setNb_targets(nb_targets);
		setNb_traffic_packets(nb_traffic_packets);
		
	
	}
}
