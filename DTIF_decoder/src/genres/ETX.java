package genres;

import main_package.*;

public class ETX extends Value{
	private int nb_words;
	
	 
	public ETX() {super();}

	public ETX(String value){
		super(value);
	}
	
public int getNb_words() {
		return nb_words;
	}

public int getParity() {
	return parity;
}

public void setParity(int parity) {
	this.parity = parity;
}

public void setNb_words(int nb_words) {
		this.nb_words = nb_words;
	}


public void decryptValue() {
		
		// extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
		int the_parity = Character.getNumericValue(bits.charAt(0));
		
				
		// extracting the 12 bits containing number of words value (bit number 9 to bit number 20 in arinc735b) in string format
		String the_nb_words = bits.substring(12, 24); 
		
		// converting the string extracted containing the 12 bits to decimal value using the function parseInt already existing in java
		nb_words = Integer.parseInt(the_nb_words, 2);
		
		/* setting every information extracted to the adequate attribute */
		setParity(the_parity); 
		setNb_words(nb_words);
		
	}
	}

	
