package genres;

import java.time.LocalTime;

import main_package.*;

public class Packet_header extends Value{
	private LocalTime time;
	private int traffic_nb;
	private int nb_labels_packet;
	private String status;
	private String display_matrix;
	private String Source_data;
	
	

	public Packet_header() {super();}

	public Packet_header(String value){
		super(value);
	}
	
	public int getParity() {
		return parity;
	}



	public void setParity(int parity) {
		this.parity = parity;
	}



	public int getTraffic_nb() {
		return traffic_nb;
	}



	public void setTraffic_nb(int traffic_nb) {
		this.traffic_nb = traffic_nb;
	}






	public int getNb_labels_packet() {
		return nb_labels_packet;
	}

	public void setNb_labels_packet(int nb_labels_packet) {
		this.nb_labels_packet = nb_labels_packet;
	}

	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getDisplay_matrix() {
		return display_matrix;
	}



	public void setDisplay_matrix(String display_matrix) {
		this.display_matrix = display_matrix;
	}



	public String getSource_data() {
		return Source_data;
	}



	public void setSource_data(String source_data) {
		Source_data = source_data;
	}



	public void decryptValue() {
		
		// extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
		int the_parity = Character.getNumericValue(bits.charAt(0));
		
		
		// extracting status bit number 30 in arinc735b (equivalent to bit number 22 in the dtif data provided) and converting the char extracted to int
		int the_status = Character.getNumericValue(bits.charAt(2));
		
		if(the_status == 0) {
			status = "Traffic is Airborne";
		}else if(the_status == 1) {
			status = "Traffic is on Ground";
		}
		
		// extracting the 3 bits containing Source Data Type (bit number 27 to bit number 29  in arinc735b) in string format
		String the_source_data = bits.substring(3, 6); 
		
		// converting the string extracted containing the 3 bits to decimal value using the function parseInt already existing in java
		int the_source_data_conv = Integer.parseInt(the_source_data, 2);
		
		if(the_source_data_conv == 0) {
			Source_data = "Invalid";
		}else if(the_source_data_conv == 1) {
			Source_data = "TCAS";
		}else if(the_source_data_conv == 2) {
			Source_data = "ADS-B";
		}else if(the_source_data_conv == 3) {
			Source_data = "ADS-R";
		}else if(the_source_data_conv == 4) {
			Source_data = "TISB";
		}else if(the_source_data_conv == 5 || the_source_data_conv == 6) {
			Source_data = "Reserved";
		}else if(the_source_data_conv == 7) {
			Source_data = "Multi Source Traffic Data";
		}else {
			Source_data = "NOT DEFINED";
		}
		
		
		// extracting the 4 bits containing Display Matrix (bit number 23 to bit number 26  in arinc735b) in string format
		String the_matrix = bits.substring(6, 10); 		
		
		// converting the string extracted containing the 3 bits to decimal value using the function parseInt already existing in java
		int the_matrix_conv = Integer.parseInt(the_matrix, 2);
				
		if(the_matrix_conv == 0) {
			display_matrix = "Non Threat Traffic";
		}else if(the_matrix_conv == 1) {
			display_matrix = "Traffic Advisory";
		}else if(the_matrix_conv == 2) {
			display_matrix = "Resolution Advisory";
		}else if(the_matrix_conv == 3) {
			display_matrix = "Proximate Traffic";
		}else if(the_matrix_conv == 4 || the_matrix_conv == 5 ||the_matrix_conv == 6 ||the_matrix_conv == 7) {
			display_matrix = "Reserved";
		}else if(the_matrix_conv >= 8 && the_matrix_conv <= 15 ) {
			display_matrix = "Reserved(Military)";
		}else {
					display_matrix = "NOT DEFINED";
				}
				
		// extracting the 5 bits containing number of labels per packet (bit number 16 to bit number 20 in arinc735b) in string format
		String the_labels = bits.substring(12, 17); 
				
		// converting the string extracted containing the 5 bits to decimal value using the function parseInt already existing in java
		nb_labels_packet = Integer.parseInt(the_labels, 2);
		
		
		// extracting the 7 bits containing traffic number (bit number 9 to bit number 15 in arinc735b) in string format
		String the_traffic = bits.substring(17, 24); 
						
		// converting the string extracted containing the 7 bits to decimal value using the function parseInt already existing in java
		traffic_nb = Integer.parseInt(the_traffic, 2);
		
		/* setting every information extracted to the adequate attribute */
		setParity(the_parity); 
		setStatus(status);
		setSource_data(Source_data);
		setDisplay_matrix(display_matrix);
		setNb_labels_packet(nb_labels_packet);
		setTraffic_nb(traffic_nb);
		
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}
}
