package genres;

import main_package.*;


public class STX extends Value{
private int nb_words;
private String all_traffic_bit;

public STX() {super();}

public STX(String value){
	super(value);
}
 
public int getParity() {
	return parity;
}

public void setParity(int parity) {
	this.parity = parity;
}

public int getNb_words() {
	return nb_words;
}

public void setNb_words(int nb_words) {
	this.nb_words = nb_words;
}

public String getAll_traffic_bit() {
	return all_traffic_bit;
}

public void setAll_traffic_bit(String all_traffic) {
	this.all_traffic_bit = all_traffic;
}

public void decryptValue() {
	
	// extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
	int the_parity = Character.getNumericValue(bits.charAt(0));
	
	// extracting all_traffic_bit (bit number 24 in arinc735b) and converting the char extracted to int
	int the_all_traffic_bit = Character.getNumericValue(bits.charAt(8));
	
	if(the_all_traffic_bit == 0) {
		all_traffic_bit = "Display traffic only if a TA or RA is active";
	}else if(the_all_traffic_bit == 1) {
		all_traffic_bit = "Display Traffic";
	}else {
		all_traffic_bit = "Invalid Value";
	}
	
	
	// extracting the 12 bits containing number of words value (bit number 9 to bit number 20 in arinc735b) in string format
	String the_nb_words = bits.substring(12, 24); 
	// converting the string extracted containing the 12 bits to decimal value using the function parseInt already existing in java
	nb_words = Integer.parseInt(the_nb_words, 2);
	
	/* setting every information extracted to the adequate attribute */
	setParity(the_parity); 
	setNb_words(nb_words);
	setAll_traffic_bit(all_traffic_bit);

}
}
