package main_package;

import genres.Header;

//import main_package.DataLine;
public class Value {
//the 24 bits contained in the value, the keyword transient indicates that a field should not be part of the serialization
protected transient String bits;  
protected  transient int parity;
private String data_type;


//public int getDetector() {
//	return detector;
//}
//
//public void setDetector(int detector) {
//	this.detector = detector;
//}

//constructor with parameter
protected Value(String value){
	setBits(value);
}

protected Value(){
}

 

public String getType() {
	return data_type;
}

public void setType(String type) {
	this.data_type = type;
}

public  void decryptValue() {};


public int getParity() {
	return parity;
}

public void setParity(int parity) {
	this.parity = parity;
}



public String getBits() {
	return bits;
}

public void setBits(String bits) {
	// remove - caracter form the 24 bits
	this.bits = bits.replaceAll("[-]","");
}


//to calculate the parity of the bits contained in the value attribute
public boolean has_odd_parity() {
	int x = Integer.parseInt(bits,2);

	int y = x ^ (x >> 1); 
    y = y ^ (y >> 2); 
    y = y ^ (y >> 4); 
    y = y ^ (y >> 8); 
    y = y ^ (y >> 16); 

	// Rightmost bit of y holds 
	// the parity value 
	// if (y&1) is 1 then parity  
	// is odd else even 
	if ((y & 1) > 0) 
	    return true; 
	return false; 
	
}




}
