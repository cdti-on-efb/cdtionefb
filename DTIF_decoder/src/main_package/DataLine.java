package main_package;

import genres.*;
import types.*;

import java.util.StringTokenizer;
import java.time.*; 

public class DataLine {
private String line;
private int item;
private int channel;
static int label;
private Value value;
private LocalTime time;
public int page;
public int version;
private int dtif_version;

public int getDtif_version() {
	return dtif_version;
}

public void setDtif_version(int dv) {
	this.dtif_version = dv;
}

public int getPage() {
	return page;
}

public void setPage(int page) {
	this.page = page;
}

//constructor with the line given in parameter
DataLine(String line){
	setLine(line);
}

public Value getValue() {
	String bits = value.getBits();
	String type = this.detect_genre_type();
	switch(type) {
	  case "STX":
		value = new STX(bits);
	    break;
	  case "ETX":
		value = new ETX(bits);
	    break;
	  case "Packet_header":
		  value = new Packet_header(bits);
		    break;
//	  case "Header":
//		  value = new Header(bits);
//		    break;
	  case "Type0_1":
		  value = new Type0_1(bits);
		    break;
	  case "Type1_1":
		  value = new Type1_1(bits);
		    break;
	  case "Type2_1":
		  if(dtif_version ==0)
		  value = new Type2V0_1(bits);
		  else if(dtif_version ==1)
		  value = new Type2V1_1(bits);
		    break;
	  case "Type3_1":
		  value = new Type3_1(bits);
		    break;
	  case "Type4":
		  value = new Type4(bits);
		    break;
	  case "Type5":
		  value = new Type5(bits);
		    break;
	  case "Type7_1":
		  value = new Type7_1(bits);
		    break;
	  default:

	}

	return value;
}


//decomposing the line attribute into parts : item, channel, label, value, time
//and setting every part to the associated attribute
public void decompose() {
String[] tokens = new String[5];
StringTokenizer st = new StringTokenizer(line);  
int i=0;
while (st.hasMoreTokens() && i<5) {  
tokens[i]=st.nextToken();
i++;
}  
setItem(Integer.parseInt(tokens[0]));
setChannel(Integer.parseInt(tokens[1]));
setLabel(Integer.parseInt(tokens[2]));
setTime(tokens[4]);
setValue(tokens[3]); 
}


public String getLine() {
	return line;
}


public void setLine(String line) {
	this.line = line;
}


public int getItem() {
	return item;
}


public void setItem(int item) {
	this.item = item;
}


public int getChannel() {
	return channel;
}


public void setChannel(int channel) {
	this.channel = channel;
}


public int getLabel() {
	return label;
}


public void setLabel(int label) {
	DataLine.label = label;
}




public void setValue(String aValue) {

	Value v = new Value(aValue);
	value = v;


}

public String getValue_bits() {
	return value.getBits();
}



public LocalTime getTime() {
	return time;
}

//Splitting the time to be set, to remove the first unnecessary part, 
//and be able to convert the String value of the time into LocalTime format
public void setTime(String aTime) {
	String[] splitTime = aTime.split(":",2);
	String removeFirstElement = splitTime[1];
	this.time = LocalTime.parse(removeFirstElement);
}





//detects the genre and the type of the Value and returns string corresponding to the genre or type
public String detect_genre_type() {
	if(label==367) {
		//extracting detector bit number 25 in arinc735b (equivalent to bit number 17 in the dtif data provided) and converting the char extracted to int
		int the_detector = Character.getNumericValue((value.bits).charAt(7));
		if(the_detector == 0) {
			value.setType("STX");
			return "STX";
		    }else if (the_detector == 1) {
		    	value.setType("ETX");
		    	return "ETX";
		    }else{
		    	return "Invalid label 367";
			  }
	}else if(label == 366){
			//extracting pad bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) and converting the char extracted to int
			int pad = Character.getNumericValue((value.bits).charAt(1));
			
			// extracting the 5 bits containing Pad/Manufacturing Use(bit number 19 to bit number 23 in arinc735b) in string format
			String the_bit = (value.bits).substring(9, 14);
			
			// converting the string extracted containing the 5 bits to decimal value using the function parseInt already existing in java
			int bit = Integer.parseInt(the_bit, 2);
			
			// extracting the 3 bits containing Version (bit number 16 to bit number 18 in arinc735b) in string format
			String the_version= (value.bits).substring(14,17);
			
			// converting the string extracted containing the 3 bits to decimal value using the function parseInt already existing in java
			version = Integer.parseInt(the_version, 2);
			
			// extracting the 5 bits containing Pad/Manufacturing Use(bit number 19 to bit number 23 in arinc735b) in string format
			String the_reserve_bit = (value.bits).substring(10, 12);
			
			// converting the string extracted containing the 5 bits to decimal value using the function parseInt already existing in java
			int reserve_bit = Integer.parseInt(the_reserve_bit, 2);
			
		
					// extracting the 4 bits containing Data Type (bit number 9 to bit number 12 in arinc735b) in string format
					String the_type = (value.bits).substring(20, 24); 
			
					//   converting the string extracted containing the 4 bits to decimal value using the function parseInt already existing in java
					int type_detector = Integer.parseInt(the_type, 2);
					
					if(type_detector == 0) {
						value.setType("Type0_1");
				    	return "Type0_1";
					}else if(type_detector == 1) {
						value.setType("Type1_1");
				    	return "Type1_1";
					}else if(type_detector == 3) {
						value.setType("Type3_1");
				    	return "Type3_1";
					}else if(type_detector == 4) {
						value.setType("Type4");
				    	return "Type4";
					}else if(type_detector == 5) {
						value.setType("Type5");
				    	return "Type5";
					}else if(type_detector == 7) {
						value.setType("Type7_1");
				    	return "Type7_1";
					}else if(type_detector == 2) {	
//							value.setType("Type2_1");
					    	return "Type2_1";
							}
					else if (type_detector > 7) { 
						value.setType("Packet_header");
						return "Packet_header";
					}
					else{
//							  throw new IllegalArgumentException("INVALID VALUE");
								return "INVALID VALUE";
							}
					
			
		}else {		
			value.setType("INVALID VALUE");
			return "INVALID VALUE";
		}
	}

}
