package main_package;

import genres.*;
import types.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class Dtif_decoder {
private File tcas_data;
private DataLine nextLine;


//Dtif_decoder(String file_path){
////	try {
////		  tcas_data = new File(file_path);
////	      Scanner myReader = new Scanner(tcas_data);
////	      while (myReader.hasNextLine()) {
////	      String data = myReader.nextLine();
////	      
////	      System.out.println(data);
////	      }
////	      myReader.close();
////	    } catch (FileNotFoundException e) {
////	      System.out.println("Error : File couldn't be read correctly!");
////	      e.printStackTrace();
////	    }
////	
//
//}


public Value decodeNextLine(DataLine dataline) {
	Value v = dataline.getValue();
	String type = dataline.detect_genre_type();
	
	switch(type) {
	  case "STX":
		  	((STX)v).decryptValue();
	    break;
	  case "ETX":
		  	((ETX)v).decryptValue();
	    break;
	  case "Packet_header":
			((Packet_header)v).decryptValue();
		    break;
	  case "Header":
			((Header)v).decryptValue();
		    break;
	  case "Type0_1":
			((Type0_1)v).decryptValue();
		    break;
	  case "Type1_1":
			((Type1_1)v).decryptValue();
		    break;
	  case "Type2V0_1":
			((Type2V0_1)v).decryptValue();
			break;
	  case "Type2V1_1":
			((Type2V1_1)v).decryptValue();
		    break;
	  case "Type3_1":
			((Type3_1)v).decryptValue();
		    break;
	  case "Type4":
			((Type4)v).decryptValue();
		    break;
	  case "Type5":
			((Type5)v).decryptValue();
		    break;
//	  case "Type6":
//			((Type6)v).decryptValue();
//		    break;
	  case "Type7_1":
			((Type7_1)v).decryptValue();
		    break;
	  default:

	}
    return v;
}




}
