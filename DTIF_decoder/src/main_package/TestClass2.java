package main_package;
import types.*;
import genres.*;
//
public class TestClass2 {
	
	public static void main(String args[]){ 
	
//
//// test 1 : detect_type_genre() for Type3
////	DataLine dl_type3 = new DataLine("1252	3	366	 1-01-100-0011-1000-0010-0000-11 	 000:00:00:00.521133	HI SSM_ERROR 	Octal 366	");
////	String type = dl_type3.detect_genre_type();
////	System.out.println("The type detected is : " + type); //expected "Type3"
//
////	//test of class Type5
////   Type5 type5= new Type5();
////    type5.setBits("011000100100010101110101"); //Item #92 Continuation-1
////    type5.decryptValue();
////    System.out.println("Type5 : ");
////    System.out.println("Parity :" + type5.getParity());
////    System.out.println("Position Offset:" + type5.getPosition_offset());
////    System.out.println("Width code: " + type5.getWidth_code());
////    System.out.println("length code: " +type5.getLength_code());
////    System.out.println("Length/Width Status: " + type5.getLen_wid_status());
////	
// //test of class Type3
////	Type3 type3= new Type3();
////	//type3.setBits("111000000000000011000011"); //Item #94
////	//type3.setBits("001100010000100010000011");   //Item #112
////	//type3.setBits("001100001101100010000011");   //Item #161
////	type3.setBits("010000000000000000000011");   //Item #11
////	//type3.setBits("001100001010111000000011");   //Item #32
////	//type3.setBits("001100001110101000000011");   //Item #48
////	type3.decryptValue();
////	System.out.println("Type3 : "  );
////	System.out.println("Parity :" + type3.getParity());
////	System.out.println("Page :" + type3.getPage());
////	System.out.println("Mag/True Heading:" + type3.getMag_true_heading());
////	System.out.println("Ground Speed:" + type3.getGround_speed());
////	System.out.println("Ground Speed Status:" + type3.getGround_speed_status());
////	System.out.println("Track Angle Status: " + type3.getTrack_angle_status());
////	System.out.println("Heading/Track Angle:" + type3.getTrack_angle());
////	System.out.println("Heading/Track Angle Sign:" + type3.getTrack_angle_sign());
////	System.out.println("Closure Rate:" + type3.getClosure_rate());
////	System.out.println("Closure Rate Sign:" + type3.getClosure_rate_sign());
////	System.out.println("Closure Rate Status:" + type3.getClosure_rate_status());
//	
//	
//	
//	//test of class Type0
////	Type0 type0= new Type0();
		Type0_1 type0_1 = new Type0_1();
		Type0_2 type0_2 = new Type0_2();
		Type0_3 type0_3 = new Type0_3();
		type0_1.setBits("001101011101010011100000");   //ITEM #61
		type0_2.setBits("000000001000010010110000"); //Item #62
		type0_3.setBits("010000110000100000100000"); //Item #63
////	type0.decryptValue();
		type0_1.decryptValue();
		type0_2.decryptValue();
		type0_3.decryptValue();
////	System.out.println("Type0: "  );
////	System.out.println("Parity :" + type0.getParity());
////	System.out.println("Page:" + type0.getPage());
////	System.out.println("Char1:" + type0.getChar1());
////	System.out.println("Char2:" + type0.getChar2());
////	System.out.println("Char3:" + type0.getChar3());
		System.out.println("Part1 :" + type0_1.getPart1());
////	System.out.println("Page:" + type0_1.getPage());
////	System.out.println("Char4:" + type0_1.getChar4());
////	System.out.println("Char5:" + type0_1.getChar5());
////	System.out.println("Char6:" + type0_1.getChar6());
		System.out.println("Part2 :" + type0_2.getPart2());
////	System.out.println("Char7:" + type0_2.getChar7());
////	System.out.println("Char8:" + type0_2.getChar8());
		System.out.println("Part3 :" + type0_3.getPart3());
////	System.out.println("A/C Category:" + type0_2.getAc_category());
////	System.out.println("Type Code:" + type0_2.getTypecode());
////	System.out.println("Page:" + type0_2.getPage());
////	
//	
//	//test of class Type1
////	Type1 type1= new Type1();
////	//type1.setBits("010000000000000000000001"); //Item #1
////	type1.setBits("111000000000000010110001");   //Item #33
////	//type1.setBits("000000110100110010110001");   //Item #39
////	//type1.setBits("001001000010001110000001");   //Item #42
////	type1.decryptValue();
////	System.out.println("Type1: "  );
////	System.out.println("Parity :" + type1.getParity());
////	System.out.println("Traffic Vert Sense:" + type1.getTraffic_vert_sense());
////	System.out.println("Coarse/Fine Range:" + type1.getCoarse_Fine_Range());
////	System.out.println("Traffic Range:" + type1.getTraffic_range());
////	System.out.println("Traffic Range Invalidity:" + type1.getTraffic_range_invalidity());
////	System.out.println("Relative Altitude Status:" + type1.getRelative_altitude_status());
////	System.out.println("Relative Altitude:" + type1.getRelative_altitude());
////	System.out.println("Relative Altitude Sign:" + type1.getRelative_altitude_sign());
////	System.out.println("Traffic Bearing:" + type1.getTraffic_bearing());
////	System.out.println("Traffic Bearing Sign:" + type1.getTraffic_bearing_sign());
////	System.out.println("Bearing Status:" + type1.getBearing_status());
//
//	
////	//test of class Type2V0
//	//Type2V0 type2v0= new Type2V0();
//	//type2v0.setBits("010000000000000000000010"); //Item #10 Continuation bit 1
//	//type2v0.setBits("000111110001001110010010");   //Item #45
////	type2v0.decryptValue();
////	System.out.println("Type2V0: "  );
////	System.out.println("Parity :" + type2v0.getParity());
////	System.out.println("Latitude:" + type2v0.getLatitude());
////	System.out.println("Latitude Sign:" + type2v0.getLatitude_sign());
////	System.out.println("Longitude:" + type2v0.getLongitude());
////	System.out.println("Longitude Sign:" + type2v0.getLongitude_sign());
////	System.out.println("Relative Altitude:" + type2v0.getRelative_altitude());
////	System.out.println("Relative Altitude Sign:" + type2v0.getRelative_altitude_sign());
////	System.out.println("Traffic Vertical Sense: " + type2v0.getTraffic_vertical_sense());
////	System.out.println("Relative Altitude Status" + type2v0.getRelative_altitude_status());
////	System.out.println("Page:" + type2v0.getPage());
////	
////	//test of class Type2V1
////	Type2V1 type2v1= new Type2V1();
////	//type2v1.setBits("010000000000000000000010"); //Item #10 Continuation bit 1
////	//type2v1.setBits("000111110001001110010010");   //Item #45
////		type2v1.decryptValue();
////		System.out.println("Type2V1: "  );
////		System.out.println("Page" + type2v1.getType_decimal());
////		System.out.println("Parity :" + type2v1.getParity());
////		System.out.println("Latitude:" + type2v1.getLatitude());
////		System.out.println("Latitude Sign:" + type2v1.getLatitude_sign());
////		System.out.println("Longitude:" + type2v1.getLongitude());
////		System.out.println("Longitude Sign:" + type2v1.getLongitude_sign());
////		System.out.println("Relative Altitude:" + type2v1.getVertical_speed());
////		System.out.println("Relative Altitude Sign:" + type2v1.getVertical_speed_sign());
//// 		System.out.println("Traffic Vertical Sense: " + type2v1.getVertical_speed_status());
//		
//		
//		
//		//test of class Type4
////	    Type4 type4= new Type4();
////	    type4.setBits("000000110001110101010100"); //Item #413
////	    type4.decryptValue();
////	    System.out.println("Type4 : ");
////		System.out.println("Parity :" + type4.getParity());
////		System.out.println("Page :" + type4.getPage());
////		System.out.println("AIRB QUALITY level:" + type4.getAirb_quality());
////		System.out.println("SURF QUALITY level:" + type4.getSurf_quality());
////		System.out.println("Interval MGT. QUALITY level:" + type4.getInterval_mgt_quality());
////		System.out.println("ITP QUALITY level:" + type4.getItp_quality());
////		System.out.println("TSAA QUALITY level:" + type4.getTsaa_quality());
////		System.out.println("VSA QUALITY level:" + type4.getVsa_quality());
//
//  //test of class Type7
////    Type7 type7= new Type7();
////     type7.setBits("010000000000100000110111"); //Item #219 Continuation-1  
////     //type7.setBits("100010000000011100010111"); //Item #170 Continuation-0
////     type7.decryptValue();
////     System.out.println("Type7 : ");
////     System.out.println("Parity :" + type7.getParity());
////     System.out.println("Page :" + type7.getPage());
////     System.out.println("Traffic Time:" + type7.getTraffic_time());
////     System.out.println("Hours: " + type7.getHours());
////     System.out.println("Minutes: " +type7.getMinutes());
////     System.out.println("Seconds: " + type7.getSeconds());   
////     System.out.println("Milliseconds " + type7.getMilliseconds());  
////     System.out.println("Time Application Invalidity: " + type7.getTime_application_invalidity()); 
//    
//    //test  of class STX
//    
////     	STX stx1 = new STX();
////     	stx1.setBits("100000101000011000100011");
////     	stx1.decryptValue();
////     	System.out.println("stx1 : ");
////     	System.out.println(stx1.getParity());
////     	System.out.println(stx1.getNb_words());
////     	System.out.println(stx1.getAll_traffic_bit());
//    
//     	
////    	//test  of class ETX
//
////   		ETX etx1 = new ETX();
////     		etx1.setBits("100000110000011000010101");
////     		etx1.decryptValue();
////     		System.out.println("etx : ");
////     		System.out.println(etx1.getParity());
////     		System.out.println(etx1.getNb_words());
//     		
////     		//test 7 of class Header
//
////     		Header header = new Header();
////     		//header.setBits("000000110000000000110100"); //Item34 Dtif Version 0  
////     		//header.setBits("000000110100110010110001"); //Item39 Dtif Version 1  
////     		header.setBits("101111111000000001110000");
////    		header.decryptValue();
////     		System.out.println("Header : ");
////    		System.out.println("Parity :" + header.getParity());
////    		System.out.println(header.getNb_targets());
////     		System.out.println(header.getDTIF_version());
////     		System.out.println(header.getNb_traffic_packets());     		
//     		
//     		
////     		test  of class Packet Header
////     		Packet_header packet = new Packet_header();
////     		packet.setBits("011000100111000111011000");
////     		packet.setBits("001100001110101000000011");
////     		packet.decryptValue();
////     		System.out.println("Packet Header : ");
////     		System.out.println("Parity :" + packet.getParity());
////     		System.out.println(packet.getStatus());
////     		System.out.println(packet.getSource_data());
////     		System.out.println(packet.getDisplay_matrix());
////     		System.out.println(packet.getNb_labels_packet());
////     		System.out.println(packet.getTraffic_nb());
//
	}
}
