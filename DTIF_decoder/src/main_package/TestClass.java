package main_package;
import com.google.gson.Gson;

import java.util.StringTokenizer;

import genres.*;

import types.*;

import java.io.FileWriter;
import java.io.IOException;
import java.time.*; 
//import org.json.simple.JSONObject;


public class TestClass {
public static void main(String args[]){  
	//test 1 tokenizer
//	StringTokenizer st = new StringTokenizer("2	3	366	 1-00-010-0000-0001-1100-1110-10 	 000:00:00:00.130385	HI 	Octal 366	");  
//	while (st.hasMoreTokens()) {  
//	System.out.println(st.nextToken());  
//	}  
	
	//test 2 time comparing
//	LocalTime t,t2;
//	t = LocalTime.parse("00:01:00.130023");
//	t2 = LocalTime.parse("02:00:00.130013");
//	System.out.println(t);  
//	System.out.println(t);  
//	boolean b = t.isBefore(t2);
//	System.out.println(b); 

	//test3 split time
//	String aTime= "000:00:00:00.130023";
//	String[] spliTime = aTime.split(":",2);
//	String removeFirstElement = spliTime[1];
//	LocalTime lt= LocalTime.parse(removeFirstElement);
//	System.out.println(lt);
	
	//test4 bitsToDecimal function
//	String h2="100000101000011000100011";
//	String h="100000101000011000100011".substring(12, 24);
//	int decimal = Integer.parseInt(h, 2); 
//	System.out.println(h);
//	System.out.println(decimal);
//	System.out.println("traffic bit : " + h2.charAt(8));
//	System.out.println("Parity : " + h2.charAt(0));
//	
//	//test 5 of class STX
//
//	STX stx1 = new STX();
//	stx1.setBits("100000101000011000100011");
//	stx1.decryptValue();
//	System.out.println("stx1 : ");
//	System.out.println(stx1.getParity());
//	System.out.println(stx1.getNb_words());
//	System.out.println(stx1.getAll_traffic_bit());
//
//	
//	//test 6 of class ETX
//
//	ETX etx1 = new ETX();
//	etx1.setBits("100000110000011000010101");
//	etx1.decryptValue();
//	System.out.println("etx : ");
//	System.out.println(etx1.getParity());
//	System.out.println(etx1.getNb_words());
//	
//	//test 7 of class Header
//	
//	Header header = new Header();
//	header.setBits("000001110000101000010010");
//	header.decryptValue();
//	System.out.println("Header : ");
//	System.out.println("Parity :" + header.getParity());
//	System.out.println(header.getNb_targets());
//	System.out.println(header.getDTIF_version());
//	System.out.println(header.getNb_traffic_packets());
//	
	//test 8 of class Packet Header
//	Packet_header packet = new Packet_header();
//	packet.setBits("011000100111000111011000");
//	packet.decryptValue();
//	System.out.println("Packet Header : ");
//	System.out.println("Parity :" + packet.getParity());
//	System.out.println(packet.getStatus());
//	System.out.println(packet.getSource_data());
//	System.out.println(packet.getDisplay_matrix());
//	System.out.println(packet.getNb_packets());
//	System.out.println(packet.getTraffic_nb());

	
//test of class Type7
//    Type7 type7 = new Type7();
//    type7.setBits("100010000000011100010111");//Continuation- 0
//    //type7.setBits("010000000000100000110111"); //Continuation-1
//    type7.decryptValue();
//    System.out.println("Type7 : ");
//	System.out.println("Parity :" + type7.getParity());
//	System.out.println("Traffic Time:" + type7.getTraffic_time());
//	System.out.println(type7.getTime_application_invalidity());
//	

	
	
	
//test 9 write in a file
//try {
//      FileWriter myWriter = new FileWriter("output.txt");
//      myWriter.write("Files in Java might be tricky, but it is fun enough!");
//      myWriter.close();
//      System.out.println("Successfully wrote to the file.");
//    } catch (IOException e) {
//      System.out.println("An error occurred.");
//      e.printStackTrace();
//    }

//test 10 json encoding (classic method without using gson lib)
//JSONObject o1 = new JSONObject();
//  o1.put("name", "Alex");
//  o1.put("roll", new Integer(12));
//  o1.put("total_marks", new Double(684.50));
//  o1.put("pass", new Boolean(true));
//  System.out.print(o1);

//test 11 gson test

//Gson gson = new Gson();
//
//STX staff = new STX("100000101000011000100011");
//staff.setParity(1); 
//staff.setNb_words(3157);
//staff.setAll_traffic_bit(100);
////staff.setBits("100000101000011000100011");
//staff.decryptValue();
//
//// Java objects to String
// String json = gson.toJson(staff);
//
//// Java objects to File
//try (FileWriter writer = new FileWriter("2jannuary.json")) {
//	writer.write("[");
//	writer.write(String.format("%n"));
//    gson.toJson(staff, writer);
//    writer.write(",");
//	writer.write(String.format("%n"));
//    gson.toJson(staff, writer);
//	writer.write(String.format("%n"));
//    writer.write("]");
//
//} catch (IOException e) {
//    e.printStackTrace();
//}
//  System.out.print("gson success");

  
	
	

//test 12 : testing Dtif_to_json class


Dtif_to_json dtj = new Dtif_to_json("TCAS_5Sequences.XLS", "hFEBR_5Seq.json");
dtj.convert(); 
//  
  
  
  
  
  
  
  
}
}
