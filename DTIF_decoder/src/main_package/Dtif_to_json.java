package main_package;

//import genres.*;
import com.google.gson.Gson;

import genres.*;
import types.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.Scanner;



public class Dtif_to_json {
	
private static String dtif_data_path = "";
private static String json_data_path = "";


public void convert(){ 
clearFile(); 
Dtif_decoder dtif_decoder = new Dtif_decoder();
Gson gson = new Gson();
String nextLine;
DataLine dataline;
//String decoded_data;

try (FileWriter writer = new FileWriter(json_data_path)) {
File dtif_data = new File("TCAS_cleared.XLS"); //input file
Scanner myReader = new Scanner(dtif_data);
boolean first = true;
int sequence_num = 0;
nextLine = myReader.nextLine(); //skipping the first line dedicated to only describe each column in the dtif file
System.out.println(nextLine);
writer.write("[");
writer.write(String.format("%n"));

while(myReader.hasNextLine()) { //browsing file line by line
	nextLine = myReader.nextLine();
    System.out.println(nextLine);
	dataline = new DataLine(nextLine);
	dataline.decompose(); // decomposing the dataline into value/label/time
	
	String detected_type = dataline.detect_genre_type(); //return the type or genre of the dataline
	/** declaration of the global variables used to manipulate the switch cases**/
	String[] nextlines_table = new String[3];
	DataLine my_dl;
	Value my_val;
	String my_bits;
	int dtif_version=0;
	System.out.println("SWITCHING ON THE DETECTED TYPE : " + detected_type);
	switch(detected_type) {
	//STX is always followed by DTIF header then Packet header
	//We detect STX using detect_type_genre() function then stock the two next following datalines in our table
	case "STX":
		nextlines_table[0] = nextLine;
		nextlines_table[1] = myReader.nextLine();
		nextlines_table[2] = myReader.nextLine();
		
		sequence_num++;
		writer.write("{\"Sequence number\":"+sequence_num+"}");
		writer.write(String.format("%n"));
		// STX
		System.out.println(nextlines_table[0]);
		my_dl = new DataLine(nextlines_table[0]);
		my_dl.decompose();
//		Value my_val = dtif_decoder.decodeNextLine(my_dl);
		my_val = my_dl.getValue();
		my_val.setType("STX");
	    ((STX)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n")); 
	    System.out.println("This DataLine was Successfully wrote into the json file.");

	    // DTIF HEADER
	    System.out.println(nextlines_table[1]);
		my_dl = new DataLine(nextlines_table[1]);
		my_dl.decompose();
		my_bits = my_dl.getValue_bits();
		my_val = new Header(my_bits);
		my_val.setType("Header");
	    ((Header)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n"));
		dtif_version = ((Header)my_val).getDTIF_version();
		System.out.println("DTIF version is " + dtif_version);
	    System.out.println("This DataLine was Successfully wrote into the json file.");
	    
	    //PACKET HEADER
	    System.out.println(nextlines_table[2]);
		my_dl = new DataLine(nextlines_table[2]);
		my_dl.decompose();
		my_bits = my_dl.getValue_bits();
		my_val = new Packet_header(my_bits);
		LocalTime lt = my_dl.getTime();
		my_val.setType("Packet_header");
	    ((Packet_header)my_val).decryptValue();
	    ((Packet_header)my_val).setTime(lt);
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n"));
	    System.out.println("This DataLine was Successfully wrote into the json file.");
	    
	break;
		
		
	// ETX is the last dataline which ends the date sequence
	case "ETX":
		System.out.println(nextLine);
		my_dl = new DataLine(nextLine);
		my_dl.decompose();
		my_val = my_dl.getValue();
		my_val.setType("ETX");
	    ((ETX)my_val).decryptValue();
		gson.toJson(my_val, writer);
	    System.out.println("This DataLine was Successfully wrote into the json file.");    
	break;
	
	//Type 0 has 3 pages : Type0_1 followed by Type0_2 and Type0_3 
	// We detect Type0_1 first then call nextLine twice and stock the dataline in our nextlines_table
	case "Type0_1":
		nextlines_table[0] = nextLine;
		nextlines_table[1] = myReader.nextLine();
		nextlines_table[2] = myReader.nextLine();
		
		// Type0_1
		System.out.println(nextlines_table[0]);
		my_dl = new DataLine(nextlines_table[0]);
		my_dl.decompose();
		my_val = my_dl.getValue();
		my_val.setType("Type0_1");
	    ((Type0_1)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n")); 
	    System.out.println("This DataLine was Successfully wrote into the json file.");

	    // Type0_2
	    System.out.println(nextlines_table[1]);
		my_dl = new DataLine(nextlines_table[1]);
		my_dl.decompose();
		my_bits = my_dl.getValue_bits();
		my_val = new Type0_2(my_bits);
		my_val.setType("Type0_2");
	    ((Type0_2)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n"));
	    System.out.println("This DataLine was Successfully wrote into the json file.");
	    
	    // Type0_3
	    System.out.println(nextlines_table[2]);
		my_dl = new DataLine(nextlines_table[2]);
		my_dl.decompose();
		my_bits = my_dl.getValue_bits();
		my_val = new Type0_3(my_bits);
		my_val.setType("Type0_3");
	    ((Type0_3)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n"));
	    System.out.println("This DataLine was Successfully wrote into the json file.");
	    
	break;

	//Type 1 has 2 pages : Type1_1 followed by Type1_2
	// We detect Type1_1 first then call nextLine  and stock the dataline in our nextlines_table
	case "Type1_1":
		nextlines_table[0] = nextLine;
		nextlines_table[1] = myReader.nextLine();
		
		// Type1_1
		System.out.println(nextlines_table[0]);
		my_dl = new DataLine(nextlines_table[0]);
		my_dl.decompose();
		my_val = my_dl.getValue();
		my_val.setType("Type1_1");
	    ((Type1_1)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n")); 
	    System.out.println("This DataLine was Successfully wrote into the json file.");

	    // Type1_2
	    System.out.println(nextlines_table[1]);
		my_dl = new DataLine(nextlines_table[1]);
		my_dl.decompose();
		my_bits = my_dl.getValue_bits();
		my_val = new Type1_2(my_bits);
		my_val.setType("Type1_2");
	    ((Type1_2)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n"));
	    System.out.println("This DataLine was Successfully wrote into the json file.");
	    
	break;
	
	//Type 3 has 2 pages : Type3_1 followed by Type3_2
	// We detect Type3_1 first then call nextLine  and stock the dataline in our nextlines_table
	case "Type3_1":
		nextlines_table[0] = nextLine;
		nextlines_table[1] = myReader.nextLine();
		
		// Type3_1
		System.out.println(nextlines_table[0]);
		my_dl = new DataLine(nextlines_table[0]);
		my_dl.decompose();
		my_val = my_dl.getValue();
		my_val.setType("Type3_1");
	    ((Type3_1)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n")); 
	    System.out.println("This DataLine was Successfully wrote into the json file.");

	    // Type3_2
	    System.out.println(nextlines_table[1]);
		my_dl = new DataLine(nextlines_table[1]);
		my_dl.decompose();
		my_bits = my_dl.getValue_bits();
		my_val = new Type3_2(my_bits);
		my_val.setType("Type3_2");
	    ((Type3_2)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n"));
	    System.out.println("This DataLine was Successfully wrote into the json file.");
	    
	break;
	
	//Type 4 has 2 pages : Type4_1 followed by Type4_2
	// We detect Type1_1 first then call nextLine
	// we neglect the second page because it has no relevant information 
	// since it is reserved by arinc735 for future using
	case "Type4":
		nextlines_table[0] = nextLine;
		nextlines_table[1] = myReader.nextLine(); // to neglect type4 second page which contains no relevant information
		//Type4 first page we consider only
		System.out.println(nextLine);
		my_dl = new DataLine(nextLine);
		my_dl.decompose();
		my_val = my_dl.getValue();
		my_val.setType("Type4");
	    ((Type4)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n")); 
		writer.write("{\"Type4_page2\":\"Reserved by Arinc735b for future application's quality level\"}");
		writer.write(",");
		writer.write(String.format("%n")); 
	    System.out.println("This DataLine was Successfully wrote into the json file.");    
	
	break;
	
	//Type 5 has only one page detected, and is followed by a packet header
	case "Type5":
		nextlines_table[0] = nextLine;
		nextlines_table[1] = myReader.nextLine();
		
		System.out.println(nextlines_table[0]);
		my_dl = new DataLine(nextlines_table[0]);
		my_dl.decompose();
		my_val = my_dl.getValue();
		my_val.setType("Type5");
	    ((Type5)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n")); 
	    System.out.println("This DataLine was Successfully wrote into the json file.");    
	    
	    //PACKET HEADER
	    System.out.println(nextlines_table[1]);
		my_dl = new DataLine(nextlines_table[1]);
		my_dl.decompose();
		my_bits = my_dl.getValue_bits();
		int my_label = my_dl.getLabel();
		if(my_label == 366) {
		my_val = new Packet_header(my_bits);
		LocalTime lt3 = my_dl.getTime();
		my_val.setType("Packet_header");
	    ((Packet_header)my_val).decryptValue();
	    ((Packet_header)my_val).setTime(lt3);
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n"));
	    System.out.println("This DataLine was Successfully wrote into the json file.");
		}
		else if (my_label == 367) {
			my_val = new ETX(my_bits);
			my_val.setType("ETX");
		    ((ETX)my_val).decryptValue();
			gson.toJson(my_val, writer);
//			writer.write(",");
			writer.write(String.format("%n")); 
		    System.out.println("This DataLine was Successfully wrote into the json file.");
		}
	
	break;
	
	//Type 7 has 2 pages : Type7_1 followed by Type7_2
		// We detect Type7_1 first then call nextLine  and stock the dataline in our nextlines_table
	case "Type7_1":
		nextlines_table[0] = nextLine;
		nextlines_table[1] = myReader.nextLine();
		
		// Type7_1
		System.out.println(nextlines_table[0]);
		my_dl = new DataLine(nextlines_table[0]);
		my_dl.decompose();
		my_val = my_dl.getValue();
		my_val.setType("Type7_1");
	    ((Type7_1)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n")); 
	    System.out.println("This DataLine was Successfully wrote into the json file.");

	    // Type7_2
	    System.out.println(nextlines_table[1]);
		my_dl = new DataLine(nextlines_table[1]);
		my_dl.decompose();
		my_bits = my_dl.getValue_bits();
		my_val = new Type7_2(my_bits);
		my_val.setType("Type7_2");
	    ((Type7_2)my_val).decryptValue();
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n"));
	    System.out.println("This DataLine was Successfully wrote into the json file.");
	    
	break;
	
	//Packet header has only one page
	case "Packet_header":
		System.out.println(nextLine);
		my_dl = new DataLine(nextLine);
		my_dl.decompose();
		my_val = my_dl.getValue();
		LocalTime lt2 = my_dl.getTime();
		my_val.setType("Packet_header");
	    ((Packet_header)my_val).decryptValue();
	    ((Packet_header)my_val).setTime(lt2);
		gson.toJson(my_val, writer);
		writer.write(",");
		writer.write(String.format("%n")); 
	    System.out.println("This DataLine was Successfully wrote into the json file.");    
	
	break;
	
	// We have 2 different versions for Type2, and the version number is stocked in DTIF header
	// We extract this information from DTIF Header and put in in variable dtif_verion
	//According to the adequate version, a different decryptValue() function is called
	case "Type2_1":
		nextlines_table[0] = nextLine;
		nextlines_table[1] = myReader.nextLine();
		nextlines_table[2] = myReader.nextLine();
		
		
		// In case Type2V0 is detected
		//Type2V0 has 3 pages
		// We detect Type2V0_1 first then call nextLine twice and stock the dataline in our nextlines_table
		if(dtif_version==0) {
			
			// Type2V0_1
			System.out.println(nextlines_table[0]);
			my_dl = new DataLine(nextlines_table[0]);
			my_dl.decompose();
			my_dl.setDtif_version(0);
			my_val = my_dl.getValue();
			my_val.setType("Type2V0_1");
		    ((Type2V0_1)my_val).decryptValue();
			gson.toJson(my_val, writer);
			writer.write(",");
			writer.write(String.format("%n")); 
		    System.out.println("This DataLine was Successfully wrote into the json file.");

		    // Type2V0_2
		    System.out.println(nextlines_table[1]);
			my_dl = new DataLine(nextlines_table[1]);
			my_dl.decompose();
			my_bits = my_dl.getValue_bits();
			my_val = new Type2V0_2(my_bits);
			my_val.setType("Type2V0_2");
		    ((Type2V0_2)my_val).decryptValue();
			gson.toJson(my_val, writer);
			writer.write(",");
			writer.write(String.format("%n"));
		    System.out.println("This DataLine was Successfully wrote into the json file.");
		    
		    // Type2V0_3
		    System.out.println(nextlines_table[2]);
			my_dl = new DataLine(nextlines_table[2]);
			my_dl.decompose();
			my_bits = my_dl.getValue_bits();
			my_val = new Type2V0_3(my_bits);
			my_val.setType("Type2V0_3");
		    ((Type2V0_3)my_val).decryptValue();
			gson.toJson(my_val, writer);
			writer.write(",");
			writer.write(String.format("%n"));
		    System.out.println("This DataLine was Successfully wrote into the json file.");
			
		}
		
		// In case Type2V1 is detected
		//Type2V1 has 3 pages
		// We detect Type2V0_1 first then call nextLine twice and stock the dataline in our nextlines_table
		else if (dtif_version==1) {
			// Type2V1_1
			System.out.println(nextlines_table[0]);
			my_dl = new DataLine(nextlines_table[0]);
			my_dl.decompose();
			my_dl.setDtif_version(1);
			my_val = my_dl.getValue();
			my_val.setType("Type2V1_1");
		    ((Type2V1_1)my_val).decryptValue();
			gson.toJson(my_val, writer);
			writer.write(",");
			writer.write(String.format("%n")); 
		    System.out.println("This DataLine was Successfully wrote into the json file.");

		    // Type2V1_2
		    System.out.println(nextlines_table[1]);
			my_dl = new DataLine(nextlines_table[1]);
			my_dl.decompose();
			my_bits = my_dl.getValue_bits();
			my_val = new Type2V1_2(my_bits);
			my_val.setType("Type2V1_2");
		    ((Type2V1_2)my_val).decryptValue();
			gson.toJson(my_val, writer);
			writer.write(",");
			writer.write(String.format("%n"));
		    System.out.println("This DataLine was Successfully wrote into the json file.");
		    
		    // Type2V1_3
		    System.out.println(nextlines_table[2]);
			my_dl = new DataLine(nextlines_table[2]);
			my_dl.decompose();
			my_bits = my_dl.getValue_bits();
			my_val = new Type2V1_3(my_bits);
			my_val.setType("Type2V1_3");
		    ((Type2V1_3)my_val).decryptValue();
			gson.toJson(my_val, writer);
			writer.write(",");
			writer.write(String.format("%n"));
		    System.out.println("This DataLine was Successfully wrote into the json file.");
			
		}
		
		
	    
	break;
	
	 default:

	}
	
}


writer.write(String.format("%n"));  //go to the next line in json file
writer.write("]");
myReader.close();
writer.close();
} 
catch (FileNotFoundException e) {
System.out.println("Error : File couldn't be read correctly!");
e.printStackTrace();
}
catch (IOException e) {
    System.out.println("IOException");
    e.printStackTrace();
}
}




Dtif_to_json(String dtif_data_path, String json_data_path){
	setDtif_data_path(dtif_data_path);
	setJson_data_path(json_data_path);
}

public static String getDtif_data_path() {
	return dtif_data_path;
}

public static void setDtif_data_path(String dtif_data_path) {
	Dtif_to_json.dtif_data_path = dtif_data_path;
}

public static String getJson_data_path() {
	return json_data_path;
}

public static void setJson_data_path(String json_data_path) {
	Dtif_to_json.json_data_path = json_data_path;
}


public void clearFile(){ 
String nextLine;
DataLine dataline;

try (FileWriter writer = new FileWriter("TCAS_cleared.XLS")) {
File dtif_data = new File(dtif_data_path); //input file
Scanner myReader = new Scanner(dtif_data);
nextLine = myReader.nextLine(); //skipping the first line dedicated to only describe each column in the dtif file
writer.write(nextLine);
writer.write(String.format("%n"));
//System.out.println(nextLine);

while(myReader.hasNextLine()) { //browsing file
	nextLine = myReader.nextLine();
//    System.out.println(nextLine);
	dataline = new DataLine(nextLine);
	dataline.decompose();
	
	int calculated_parity = 0;
	if (dataline.getValue().has_odd_parity()) calculated_parity =0;
	else calculated_parity =1;
	
	//First, check that the odd parity is correct
	//Second, filter to consider only channel 3 and labels 366 and 367
	if(dataline.getChannel()==3 
	   && ( (dataline.getLabel()==366 && calculated_parity==(dataline.getValue().getParity())) 
	   ||  dataline.getLabel()==367)) 
	{
		
		writer.write(nextLine);
		writer.write(String.format("%n"));
//		Value val = dtif_decoder.decodeNextLine(dataline); //decoding line
	}	
}
myReader.close();
writer.close();
} 
catch (FileNotFoundException e) {
System.out.println("Error : File couldn't be read correctly!");
e.printStackTrace();
}
catch (IOException e) {
    System.out.println("IOException");
    e.printStackTrace();
}
}

}