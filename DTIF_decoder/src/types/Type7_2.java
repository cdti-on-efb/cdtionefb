package types;

import genres.Data_word;



public class Type7_2 extends Data_word{
	
	
	private String time_application_invalidity;
	private transient int continuation_bit;
	private float milliseconds;
	
	
	public Type7_2() {super();}

	public Type7_2(String value){
		super(value);
	}
	
	public float getMilliseconds() {
		return milliseconds;
	}


	public void setMilliseconds(float milliseconds) {
		this.milliseconds = milliseconds;
	}


	
	public int getContinuation_bit() {
		return continuation_bit;
	}

	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}


	public String getTime_application_invalidity() {
		return time_application_invalidity;
	}

	public void setTime_application_invalidity(String time_application_invalidity) {
		this.time_application_invalidity = time_application_invalidity;
	}


	public void decryptValue() {
		
		
		// extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
		int the_parity = Character.getNumericValue(bits.charAt(0));
		
		// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
		int continuation_bit = Character.getNumericValue(bits.charAt(1));
		
		setContinuation_bit(continuation_bit);
		
		//To check the data type termination
		if(continuation_bit == 1){
			
		
			// extracting the 13 bits containing milliseconds(bit number 16 to bit number 28 in arinc735b) in string format
			String the_milliseconds = bits.substring(4, 17);
						
			// converting the string extracted containing the 13 bits to decimal value using the function parseInt already existing in java
			milliseconds = Float.parseFloat(the_milliseconds);
			
			// extracting Time of Applicability Invalidity bit number 29 in arinc735b (equivalent to bit number 21 in the dtif data provided) and converting the char extracted to int
			int the_timeapp_invalidity = Character.getNumericValue(bits.charAt(3));
			
			//To determine in understandable Format
			if(the_timeapp_invalidity == 0) {
				time_application_invalidity = "Valid";
			}else if(the_timeapp_invalidity == 1) {
				time_application_invalidity = "Invalid";
			}else {
				time_application_invalidity = "NOT DEFINED";
			}
			
			/* setting every information extracted to the adequate attribute */
			setParity(the_parity); 
			//setTraffic_time(traffic_time);
			setMilliseconds(milliseconds);
			setTime_application_invalidity(time_application_invalidity);
			
			
		  }else {
			  throw new IllegalArgumentException("INVALID VALUE");
		  }
		
		//traffic_time = (hours + ":" + minutes + ":" + seconds + ":" + milliseconds);
		
		
		
		}
	
}
		
			
		
		
				
	
