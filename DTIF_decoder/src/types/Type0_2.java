/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;
import genres.Data_word;
/**
 *
 * @author Harshit
 */
public class Type0_2 extends Data_word{
private transient int continuation_bit;
private transient String char4;
private transient String char5;
private transient String char6;
private String ID_part2;



public String getPart2() {
	return ID_part2;
}

public void setPart2(String part2) {
	this.ID_part2 = part2;
}

public String getChar4() {
	return char4;
}

public void setChar4(String char4) {
	this.char4 = char4;
}

public String getChar5() {
	return char5;
}

public String getChar6() {
    return char6;
    }

public void setChar6(String char6) {
    this.char6 = char6;
    }

public void setChar5(String char5) {
	this.char5 = char5;
}
	public Type0_2() {super();}

	public Type0_2(String value){
		super(value);
	}
	


	public int getContinuation_bit() {
		return continuation_bit;
	}

	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}
    @Override
    public void decryptValue() {
        // extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
	continuation_bit = Character.getNumericValue(bits.charAt(1));
							
	setContinuation_bit(continuation_bit);
	
	//To check the data type termination
	
	//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
	int the_parity = Character.getNumericValue(bits.charAt(0));
	
	/* setting every information extracted to the adequate attribute */
	setParity(the_parity);
        
        //character 4,5&6
		
        // extracting the 6 bits containing Character 4(bit number 9 to bit number 14 in arinc735b) in string format
	String the_char4 = bits.substring(18 ,24);
	if(the_char4.equals("000000")){
    	char4 = "";
    }
    else if(the_char4.equals("100000")){
    	char4 = "A";
    }else if(the_char4.equals("010000")){
    	char4 = "B";
    }else if(the_char4.equals("110000")){
    	char4 = "C";
    }else if(the_char4.equals("001000")){
    	char4 = "D";
    }else if(the_char4.equals("101000")){
    	char4 = "E";
    }else if(the_char4.equals("011000")){
    	char4 = "F";
    }else if(the_char4.equals("111000")){
    	char4 = "G";
    }else if(the_char4.equals("000100")){
    	char4 = "H";
    }else if(the_char4.equals("100100")){
    	char4 = "I";
    }else if(the_char4.equals("010100")){
    	char4 = "J";
    }else if(the_char4.equals("110100")){
    	char4 = "K";
    }else if(the_char4.equals("001100")){
    	char4 = "L";
    }else if(the_char4.equals("101100")){
    	char4 = "M";
    }else if(the_char4.equals("011100")){
    	char4 = "N";
    }else if(the_char4.equals("111100")){
    	char4 = "O";
    }else if(the_char4.equals("000010")){
    	char4 = "P";
    }else if(the_char4.equals("100010")){
    	char4 = "Q";
    }else if(the_char4.equals("010010")){
    	char4 = "R";
    }else if(the_char4.equals("110010")){
    	char4 = "S";
    }else if(the_char4.equals("001010")){
    	char4 = "T";
    }else if(the_char4.equals("101010")){
    	char4 = "U";
    }else if(the_char4.equals("011010")){
    	char4 = "V";
    }else if(the_char4.equals("111010")){
    	char4 = "W";
    }else if(the_char4.equals("000110")){
    	char4 = "X";
    }else if(the_char4.equals("100110")){
    	char4 = "Y";
    }else if(the_char4.equals("010110")){
    	char4 = "Z";
    }else if(the_char4.equals("000001")){
    	char4 = "SP";
    }else if(the_char4.equals("000011")){
    	char4 = "0";
    }else if(the_char4.equals("100011")){
    	char4 = "1";
    }else if(the_char4.equals("010011")){
    	char4 = "2";
    }else if(the_char4.equals("110011")){
    	char4 = "3";
    }else if(the_char4.equals("001011")){
    	char4 = "4";
    }else if(the_char4.equals("101011")){
    	char4 = "5";
    }else if(the_char4.equals("011011")){
    	char4 = "6";
    }else if(the_char4.equals("111011")){
    	char4 = "7";
    }else if(the_char4.equals("000111")){
    	char4 = "8";
    }else if(the_char4.equals("100111")){
    	char4 = "9";
    }
					
		
        // extracting the 6 bits containing Character 5(bit number 15 to bit number 20 in arinc735b) in string format
	String the_char5 = bits.substring(12 ,18);
						
	if(the_char5.equals("000000")){
    	char5 = "";
    }
    else if(the_char5.equals("100000")){
    	char5 = "A";
    }else if(the_char5.equals("010000")){
    	char5 = "B";
    }else if(the_char5.equals("110000")){
    	char5 = "C";
    }else if(the_char5.equals("001000")){
    	char5 = "D";
    }else if(the_char5.equals("101000")){
    	char5 = "E";
    }else if(the_char5.equals("011000")){
    	char5 = "F";
    }else if(the_char5.equals("111000")){
    	char5 = "G";
    }else if(the_char5.equals("000100")){
    	char5 = "H";
    }else if(the_char5.equals("100100")){
    	char5 = "I";
    }else if(the_char5.equals("010100")){
    	char5 = "J";
    }else if(the_char5.equals("110100")){
    	char5 = "K";
    }else if(the_char5.equals("001100")){
    	char5 = "L";
    }else if(the_char5.equals("101100")){
    	char5 = "M";
    }else if(the_char5.equals("011100")){
    	char5 = "N";
    }else if(the_char5.equals("111100")){
    	char5 = "O";
    }else if(the_char5.equals("000010")){
    	char5 = "P";
    }else if(the_char5.equals("100010")){
    	char5 = "Q";
    }else if(the_char5.equals("010010")){
    	char5 = "R";
    }else if(the_char5.equals("110010")){
    	char5 = "S";
    }else if(the_char5.equals("001010")){
    	char5 = "T";
    }else if(the_char5.equals("101010")){
    	char5 = "U";
    }else if(the_char5.equals("011010")){
    	char5 = "V";
    }else if(the_char5.equals("111010")){
    	char5 = "W";
    }else if(the_char5.equals("000110")){
    	char5 = "X";
    }else if(the_char5.equals("100110")){
    	char5 = "Y";
    }else if(the_char5.equals("010110")){
    	char5 = "Z";
    }else if(the_char5.equals("000001")){
    	char5 = "SP";
    }else if(the_char5.equals("000011")){
    	char5 = "0";
    }else if(the_char5.equals("100011")){
    	char5 = "1";
    }else if(the_char5.equals("010011")){
    	char5 = "2";
    }else if(the_char5.equals("110011")){
    	char5 = "3";
    }else if(the_char5.equals("001011")){
    	char5 = "4";
    }else if(the_char5.equals("101011")){
    	char5 = "5";
    }else if(the_char5.equals("011011")){
    	char5 = "6";
    }else if(the_char5.equals("111011")){
    	char5 = "7";
    }else if(the_char5.equals("000111")){
    	char5 = "8";
    }else if(the_char5.equals("100111")){
    	char5 = "9";
    }

     // extracting the 6 bits containing Character 6(bit number 21 to bit number 26 in arinc735b) in string format
	String the_char6 = bits.substring(6 ,12);
			
	if(the_char6.equals("000000")){
    	char6 = "";
    }
    else if(the_char6.equals("100000")){
    	char6 = "A";
    }else if(the_char6.equals("010000")){
    	char6 = "B";
    }else if(the_char6.equals("110000")){
    	char6 = "C";
    }else if(the_char6.equals("001000")){
    	char6 = "D";
    }else if(the_char6.equals("101000")){
    	char6 = "E";
    }else if(the_char6.equals("011000")){
    	char6 = "F";
    }else if(the_char6.equals("111000")){
    	char6 = "G";
    }else if(the_char6.equals("000100")){
    	char6 = "H";
    }else if(the_char6.equals("100100")){
    	char6 = "I";
    }else if(the_char6.equals("010100")){
    	char6 = "J";
    }else if(the_char6.equals("110100")){
    	char6 = "K";
    }else if(the_char6.equals("001100")){
    	char6 = "L";
    }else if(the_char6.equals("101100")){
    	char6 = "M";
    }else if(the_char6.equals("011100")){
    	char6 = "N";
    }else if(the_char6.equals("111100")){
    	char6 = "O";
    }else if(the_char6.equals("000010")){
    	char6 = "P";
    }else if(the_char6.equals("100010")){
    	char6 = "Q";
    }else if(the_char6.equals("010010")){
    	char6 = "R";
    }else if(the_char6.equals("110010")){
    	char6 = "S";
    }else if(the_char6.equals("001010")){
    	char6 = "T";
    }else if(the_char6.equals("101010")){
    	char6 = "U";
    }else if(the_char6.equals("011010")){
    	char6 = "V";
    }else if(the_char6.equals("111010")){
    	char6 = "W";
    }else if(the_char6.equals("000110")){
    	char6 = "X";
    }else if(the_char6.equals("100110")){
    	char6 = "Y";
    }else if(the_char6.equals("010110")){
    	char6 = "Z";
    }else if(the_char6.equals("000001")){
    	char6 = "SP";
    }else if(the_char6.equals("000011")){
    	char6 = "0";
    }else if(the_char6.equals("100011")){
    	char6 = "1";
    }else if(the_char6.equals("010011")){
    	char6 = "2";
    }else if(the_char6.equals("110011")){
    	char6 = "3";
    }else if(the_char6.equals("001011")){
    	char6 = "4";
    }else if(the_char6.equals("101011")){
    	char6 = "5";
    }else if(the_char6.equals("011011")){
    	char6 = "6";
    }else if(the_char6.equals("111011")){
    	char6 = "7";
    }else if(the_char6.equals("000111")){
    	char6 = "8";
    }else if(the_char6.equals("100111")){
    	char6 = "9";
    }

			
			setChar4(char4);
			setChar5(char5);
			setChar6(char6);
                  
			StringBuilder sb = new StringBuilder(14);
			if(char4!="SP" && char4!=null)	sb.append(char4);
			if(char5!="SP" && char5!=null)	sb.append(char5);
			if(char6!="SP" && char6!=null)	sb.append(char6);
			ID_part2 = sb.toString();
            setPart2(ID_part2);
        }
    
}
