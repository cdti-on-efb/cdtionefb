package types;

import genres.Data_word;



public class Type7_1 extends Data_word{
	
	private int hours;
	private int minutes;
	private float seconds;
	private  transient int continuation_bit;
	
	
	public Type7_1() {super();}

	public Type7_1(String value){
		super(value);
	}
	
	
	public int getContinuation_bit() {
		return continuation_bit;
	}

	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}


	public int getHours() {
		return hours;
	}
	
	public void setHours(int hours) {
		this.hours = hours;
	}


	public int getMinutes() {
		return minutes;
	}
	
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public float getSeconds() {
		return seconds;
	}

	public void setSeconds(float seconds2) {
		this.seconds = seconds2;
	}

	public void decryptValue() {
		
		
		// extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
		int the_parity = Character.getNumericValue(bits.charAt(0));
		
		// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
		int continuation_bit = Character.getNumericValue(bits.charAt(1));
		
		setContinuation_bit(continuation_bit);
		
		//To check the data type termination
		if(continuation_bit == 0) {
		
			// extracting the 6 bits containing seconds(bit number 13 to bit number 18 in arinc735b) in string format
			String the_seconds = bits.substring(14, 20); 
			
			// converting the string extracted containing the 6 bits to decimal value using the function parseInt already existing in java
			seconds = Float.parseFloat(the_seconds);
			
			// extracting the 6 bits containing minutes(bit number 18 to bit number 24 in arinc735b) in string format
			String the_minutes = bits.substring(8, 14); 
						
			// converting the string extracted containing the 6 bits to decimal value using the function parseInt already existing in java
			minutes = Integer.parseInt(the_minutes, 2);
			
			// extracting the 5 bits containing hours(bit number 25 to bit number 29 in arinc735b) in string format
			String the_hours = bits.substring(3, 8); 
									
			// converting the string extracted containing the 5 bits to decimal value using the function parseInt already existing in java
			hours = Integer.parseInt(the_hours, 2);
			
			/* setting every information extracted to the adequate attribute */
			setParity(the_parity); 
			//setTraffic_time(traffic_time);
			
			setHours(hours);
			setMinutes(minutes);
			setSeconds(seconds);
			//traffic_time = (hours + ":" + minutes + ":" + seconds);
			
		}else {
			  throw new IllegalArgumentException("INVALID VALUE");
		  }
		
		//traffic_time = (hours + ":" + minutes + ":" + seconds + ":" + milliseconds);
		
		
		}
	
}
		
			
		
		
				
	
