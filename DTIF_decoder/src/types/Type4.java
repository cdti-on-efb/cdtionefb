package types;

import genres.Data_word;


public class Type4 extends Data_word{


	private transient int continuation_bit;
	private String airb_quality;
	private String surf_quality;
	private String interval_mgt_quality;
	private String itp_quality;
	private String tsaa_quality;
	private String vsa_quality;
	
	
	public Type4() {super();}

	public Type4(String value){
		super(value);
	}
	
	

	public int getContinuation_bit() {
		return continuation_bit;
	}



	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}



	public String getAirb_quality() {
		return airb_quality;
	}



	public void setAirb_quality(String airb_quality) {
		this.airb_quality = airb_quality;
	}



	public String getSurf_quality() {
		return surf_quality;
	}



	public void setSurf_quality(String surf_quality) {
		this.surf_quality = surf_quality;
	}



	public String getInterval_mgt_quality() {
		return interval_mgt_quality;
	}



	public void setInterval_mgt_quality(String interval_mgt_quality) {
		this.interval_mgt_quality = interval_mgt_quality;
	}



	public String getItp_quality() {
		return itp_quality;
	}



	public void setItp_quality(String itp_quality) {
		this.itp_quality = itp_quality;
	}



	public String getTsaa_quality() {
		return tsaa_quality;
	}



	public void setTsaa_quality(String tsaa_quality) {
		this.tsaa_quality = tsaa_quality;
	}



	public String getVsa_quality() {
		return vsa_quality;
	}



	public void setVsa_quality(String vsa_quality) {
		this.vsa_quality = vsa_quality;
	}



	public void decryptValue() {
		
		// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
		int continuation_bit = Character.getNumericValue(bits.charAt(1));
				
		setContinuation_bit(continuation_bit);
				
		//To check the data type termination
		if(continuation_bit == 0) {
			
		    //extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
		    int the_parity = Character.getNumericValue(bits.charAt(0));
		    
		    setParity(the_parity); 
		   
		    //extracting the 2 bits containing AIRB Quality Level(bit number 13 to bit number 14 in arinc735b) in string format
			String the_airb_quality = bits.substring(18, 20);
			
			// converting the string extracted containing the 2 bits to decimal value using the function parseInt already existing in java
			int airb_quality_conv = Integer.parseInt(the_airb_quality, 2);
						
			//To determine the Airb Quality
			airb_quality = "";
			if(airb_quality_conv  == 0){
				airb_quality = "Invalid/Do Not Display";
			}else if(airb_quality_conv  == 1) {
				airb_quality = "Reserved";
			}else if(airb_quality_conv  == 2) {
				airb_quality = "Degraded Performance Accuracy";
			}else if(airb_quality_conv == 3) {
				airb_quality = "Good Performance Accuracy";
			}else{
				airb_quality= "INVALID VALUE";
				}
			
			//extracting the 2 bits containing SURF Quality Level(bit number 15 to bit number 16 in arinc735b) in string format
			String the_surf_quality = bits.substring(16, 18);
			
			// converting the string extracted containing the 2 bits to decimal value using the function parseInt already existing in java
			int surf_quality_conv = Integer.parseInt(the_surf_quality, 2);
						
			//To determine the Surf Quality
			surf_quality = "";
			if(surf_quality_conv  == 0){
				surf_quality = "Invalid/Do Not Display";
			}else if(surf_quality_conv  == 1) {
				surf_quality = "Reserved";
			}else if(surf_quality_conv  == 2) {
				surf_quality = "Degraded Performance Accuracy";
			}else if(surf_quality_conv == 3) {
				surf_quality = "Good Performance Accuracy";
			}else{
				surf_quality= "INVALID VALUE";
				}
						
			//extracting the 2 bits containing Interval Mgt.Quality Level(bit number 17 to bit number 18 in arinc735b) in string format
			String the_interval_mgt_quality = bits.substring(14, 16);
			
			// converting the string extracted containing the 2 bits to decimal value using the function parseInt already existing in java
			int interval_mgt_quality_conv = Integer.parseInt(the_interval_mgt_quality, 2);
						
			//To determine the Interval Mgt. Quality
			interval_mgt_quality = "";
			if(interval_mgt_quality_conv  == 0){
				interval_mgt_quality = "Invalid/Do Not Display";
			}else if(interval_mgt_quality_conv  == 1) {
				interval_mgt_quality = "Reserved";
			}else if(interval_mgt_quality_conv  == 2) {
				interval_mgt_quality = "Degraded Performance Accuracy";
			}else if(interval_mgt_quality_conv == 3) {
				interval_mgt_quality = "Good Performance Accuracy";
			}else{
				interval_mgt_quality = "INVALID VALUE";
				}
			
			//extracting the 2 bits containing ITP Quality Level(bit number 19 to bit number 20 in arinc735b) in string format
			String the_itp_quality = bits.substring(12, 14);
			
			// converting the string extracted containing the 2 bits to decimal value using the function parseInt already existing in java
			int itp_quality_conv = Integer.parseInt(the_itp_quality, 2);
						
			//To determine the ITP Quality
			itp_quality = "";
			if(itp_quality_conv  == 0){
				itp_quality = "Invalid/Do Not Display";
			}else if(itp_quality_conv  == 1) {
				itp_quality = "Reserved";
			}else if(itp_quality_conv  == 2) {
				itp_quality = "Degraded Performance Accuracy";
			}else if(itp_quality_conv == 3) {
				itp_quality = "Good Performance Accuracy";
			}else{
				itp_quality = "INVALID VALUE";
				}
					
			//extracting the 2 bits containing TSAA Quality Level(bit number 21 to bit number 22 in arinc735b) in string format
			String the_tsaa_quality = bits.substring(10, 12);

			// converting the string extracted containing the 2 bits to decimal value using the function parseInt already existing in java
			int tsaa_quality_conv = Integer.parseInt(the_tsaa_quality, 2);
						
			//To determine the TSAA Quality
			tsaa_quality = "";
			if(tsaa_quality_conv  == 0){
				tsaa_quality = "Invalid/Do Not Display";
			}else if(tsaa_quality_conv  == 1) {
				tsaa_quality = "Reserved";
			}else if(tsaa_quality_conv  == 2) {
				tsaa_quality = "Degraded Performance Accuracy";
			}else if(tsaa_quality_conv == 3) {
				tsaa_quality = "Good Performance Accuracy";
			}else{
				tsaa_quality = "INVALID VALUE";
				}
				
			//extracting the 2 bits containing VSA Quality Level(bit number 25 to bit number 26 in arinc735b) in string format
			String the_vsa_quality = bits.substring(6, 8);
			
			// converting the string extracted containing the 2 bits to decimal value using the function parseInt already existing in java
			int vsa_quality_conv = Integer.parseInt(the_vsa_quality, 2);
						
			//To determine the Vsa Quality
			vsa_quality = "";
			if(vsa_quality_conv  == 0){
				vsa_quality = "Invalid/Do Not Display";
			}else if(vsa_quality_conv  == 1) {
				vsa_quality = "Reserved";
			}else if(vsa_quality_conv  == 2) {
				vsa_quality = "Degraded Performance Accuracy";
			}else if(vsa_quality_conv == 3) {
				vsa_quality = "Good Performance Accuracy";
			}else{
				vsa_quality = "INVALID VALUE";
				}
				
			/* setting every information extracted to the adequate attribute */
			setAirb_quality(airb_quality);
			setSurf_quality(surf_quality);
			setInterval_mgt_quality(interval_mgt_quality);
			setItp_quality(itp_quality);
			setTsaa_quality(tsaa_quality);
			setVsa_quality(vsa_quality);
									
			}else {
				  throw new IllegalArgumentException("INVALID VALUE");
			  }
	
		
		}
}
