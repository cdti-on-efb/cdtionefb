/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;
import genres.Data_word;

/**
 *
 * @author Harshit
 */
public class Type0_3 extends Data_word{
private  transient int continuation_bit;
private String typecode;
private String ac_category;
private transient String char7;
private transient String char8;
private String ID_part3;


public String getPart3() {
	return ID_part3;
}

public void setPart3(String part3) {
	this.ID_part3 = part3;
}

public String getChar7() {
	return char7;
}

public void setChar7(String char7) {
	this.char7 = char7;
}

public String getChar8() {
	return char8;
}

public void setChar8(String char8) {
	this.char8 = char8;
}

	public String getAc_category() {
	return ac_category;
}

    public void setAc_category(String ac_category) {
	this.ac_category = ac_category;
}

	public String getTypecode() {
	return typecode;
}

    public void setTypecode(String typecode) {
	this.typecode = typecode;
}
    
    	public Type0_3() {super();}

	public Type0_3(String value){
		super(value);
	}
	

	public int getContinuation_bit() {
		return continuation_bit;
	}

	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}
	
    @Override
    public void decryptValue() {
        
        // extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
	continuation_bit = Character.getNumericValue(bits.charAt(1));
							
	setContinuation_bit(continuation_bit);
	
	//To check the data type termination
	
	//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
	int the_parity = Character.getNumericValue(bits.charAt(0));
	
	/* setting every information extracted to the adequate attribute */
	setParity(the_parity);
        if(continuation_bit == 1){
            //extracting character 7 & 8
			
	// extracting the 6 bits containing Character 7(bit number 9 to bit number 14 in arinc735b) in string format
	String the_char7 = bits.substring(18 ,24);
			
	if(the_char7.equals("000000")){
    	char7 = "";
    }
    else if(the_char7.equals("100000")){
    	char7 = "A";
    }else if(the_char7.equals("010000")){
    	char7 = "B";
    }else if(the_char7.equals("110000")){
    	char7 = "C";
    }else if(the_char7.equals("001000")){
    	char7 = "D";
    }else if(the_char7.equals("101000")){
    	char7 = "E";
    }else if(the_char7.equals("011000")){
    	char7 = "F";
    }else if(the_char7.equals("111000")){
    	char7 = "G";
    }else if(the_char7.equals("000100")){
    	char7 = "H";
    }else if(the_char7.equals("100100")){
    	char7 = "I";
    }else if(the_char7.equals("010100")){
    	char7 = "J";
    }else if(the_char7.equals("110100")){
    	char7 = "K";
    }else if(the_char7.equals("001100")){
    	char7 = "L";
    }else if(the_char7.equals("101100")){
    	char7 = "M";
    }else if(the_char7.equals("011100")){
    	char7 = "N";
    }else if(the_char7.equals("111100")){
    	char7 = "O";
    }else if(the_char7.equals("000010")){
    	char7 = "P";
    }else if(the_char7.equals("100010")){
    	char7 = "Q";
    }else if(the_char7.equals("010010")){
    	char7 = "R";
    }else if(the_char7.equals("110010")){
    	char7 = "S";
    }else if(the_char7.equals("001010")){
    	char7 = "T";
    }else if(the_char7.equals("101010")){
    	char7 = "U";
    }else if(the_char7.equals("011010")){
    	char7 = "V";
    }else if(the_char7.equals("111010")){
    	char7 = "W";
    }else if(the_char7.equals("000110")){
    	char7 = "X";
    }else if(the_char7.equals("100110")){
    	char7 = "Y";
    }else if(the_char7.equals("010110")){
    	char7 = "Z";
    }else if(the_char7.equals("000001")){
    	char7 = "SP";
    }else if(the_char7.equals("000011")){
    	char7 = "0";
    }else if(the_char7.equals("100011")){
    	char7 = "1";
    }else if(the_char7.equals("010011")){
    	char7 = "2";
    }else if(the_char7.equals("110011")){
    	char7 = "3";
    }else if(the_char7.equals("001011")){
    	char7 = "4";
    }else if(the_char7.equals("101011")){
    	char7 = "5";
    }else if(the_char7.equals("011011")){
    	char7 = "6";
    }else if(the_char7.equals("111011")){
    	char7 = "7";
    }else if(the_char7.equals("000111")){
    	char7 = "8";
    }else if(the_char7.equals("100111")){
    	char7 = "9";
    }

			
	// extracting the 6 bits containing Character 8(bit number 15 to bit number 20 in arinc735b) in string format
	String the_char8 = bits.substring(12 ,18);
			
	if(the_char8.equals("000000")){
    	char8 = "";
    }
    else if(the_char8.equals("100000")){
    	char8 = "A";
    }else if(the_char8.equals("010000")){
    	char8 = "B";
    }else if(the_char8.equals("110000")){
    	char8 = "C";
    }else if(the_char8.equals("001000")){
    	char8 = "D";
    }else if(the_char8.equals("101000")){
    	char8 = "E";
    }else if(the_char8.equals("011000")){
    	char8 = "F";
    }else if(the_char8.equals("111000")){
    	char8 = "G";
    }else if(the_char8.equals("000100")){
    	char8 = "H";
    }else if(the_char8.equals("100100")){
    	char8 = "I";
    }else if(the_char8.equals("010100")){
    	char8 = "J";
    }else if(the_char8.equals("110100")){
    	char8 = "K";
    }else if(the_char8.equals("001100")){
    	char8 = "L";
    }else if(the_char8.equals("101100")){
    	char8 = "M";
    }else if(the_char8.equals("011100")){
    	char8 = "N";
    }else if(the_char8.equals("111100")){
    	char8 = "O";
    }else if(the_char8.equals("000010")){
    	char8 = "P";
    }else if(the_char8.equals("100010")){
    	char8 = "Q";
    }else if(the_char8.equals("010010")){
    	char8 = "R";
    }else if(the_char8.equals("110010")){
    	char8 = "S";
    }else if(the_char8.equals("001010")){
    	char8 = "T";
    }else if(the_char8.equals("101010")){
    	char8 = "U";
    }else if(the_char8.equals("011010")){
    	char8 = "V";
    }else if(the_char8.equals("111010")){
    	char8 = "W";
    }else if(the_char8.equals("000110")){
    	char8 = "X";
    }else if(the_char8.equals("100110")){
    	char8 = "Y";
    }else if(the_char8.equals("010110")){
    	char8 = "Z";
    }else if(the_char8.equals("000001")){
    	char8 = "SP";
    }else if(the_char8.equals("000011")){
    	char8 = "0";
    }else if(the_char8.equals("100011")){
    	char8 = "1";
    }else if(the_char8.equals("010011")){
    	char8 = "2";
    }else if(the_char8.equals("110011")){
    	char8 = "3";
    }else if(the_char8.equals("001011")){
    	char8 = "4";
    }else if(the_char8.equals("101011")){
    	char8 = "5";
    }else if(the_char8.equals("011011")){
    	char8 = "6";
    }else if(the_char8.equals("111011")){
    	char8 = "7";
    }else if(the_char8.equals("000111")){
    	char8 = "8";
    }else if(the_char8.equals("100111")){
    	char8 = "9";
    }

			
			
			
	// extracting the 3 bits containing Aircraft category(bit number 25 to bit number 27 in arinc735b) in string format
	ac_category = bits.substring(5, 8); 
						
	setAc_category(ac_category);
			 
	// extracting the 3 bits containing Type Code(bit number 21 to bit number 23 in arinc735b) in string format
	typecode = bits.substring(9, 12); 
			
				
				
		setTypecode(typecode);
		setChar7(char7);
		setChar8(char8);
            
        }
        StringBuilder sb = new StringBuilder(14);
        if(char7!="SP" && char7!=null)	sb.append(char7);
		if(char8!="SP" && char8!=null)	sb.append(char8);
		ID_part3 = sb.toString();
        	setPart3(ID_part3);
    }
    
}
