package types;

import genres.Data_word;


public class Type2V1_1 extends Data_word {

	private transient int continuation_bit;
	private Float latitude1;
	
	
	public Type2V1_1() {super();}

	public Type2V1_1(String value){
		super(value);
	}

	public int getContinuation_bit() {
		return continuation_bit;
	}


	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}

	
    public Float getLatitude1() {
		return latitude1;
	}

	public void setLatitude1(Float latitude1) {
		this.latitude1 = latitude1;
	}

	/**
     *
     */
    @Override
	public void decryptValue() {
		
				// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
				int continuation_bit = Character.getNumericValue(bits.charAt(1));
								
				setContinuation_bit(continuation_bit);
								
					//To check the data type termination
					if(continuation_bit == 0) {
						
					//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
					int the_parity = Character.getNumericValue(bits.charAt(0));
							
					// extracting the 18 bits containing Latitude(bit number 13 to bit number 30 in arinc735b) in string format
					int bit1 = Character.getNumericValue(bits.charAt(2));
					int bit2 = Character.getNumericValue(bits.charAt(3));
					int bit3 = Character.getNumericValue(bits.charAt(4));
					int bit4 = Character.getNumericValue(bits.charAt(5));
					int bit5 = Character.getNumericValue(bits.charAt(6));
					int bit6 = Character.getNumericValue(bits.charAt(7));
					int bit7 = Character.getNumericValue(bits.charAt(8));
					int bit8 = Character.getNumericValue(bits.charAt(9));
					int bit9 = Character.getNumericValue(bits.charAt(10));
					int bit10 = Character.getNumericValue(bits.charAt(11));
					int bit11 = Character.getNumericValue(bits.charAt(12));
					int bit12 = Character.getNumericValue(bits.charAt(13));
					int bit13 = Character.getNumericValue(bits.charAt(14));
					int bit14 = Character.getNumericValue(bits.charAt(15));
					int bit15 = Character.getNumericValue(bits.charAt(16));
					int bit16 = Character.getNumericValue(bits.charAt(17));
					int bit17 = Character.getNumericValue(bits.charAt(18));
					int bit18 = Character.getNumericValue(bits.charAt(19));
					
					
					latitude1 = (float) (bit1*1.40625 + bit2*0.703125 + bit3*0.351563 +bit4*0.175781 + bit5*0.0878906 +bit6*0.0439453 + bit7*0.0219726 + bit8*0.0109863 +bit9*0.00549316 +bit10*0.00274658 + bit11*0.00137329 +bit12*(0.00068664551) +bit13*(3.4332275 *Math.pow(10, -4)) +bit14*(1.7166138 *Math.pow(10, -4)) +bit15*(8.5830688 *Math.pow(10, -5))+bit16*(4.2915344 *Math.pow(10, -5))+bit17*(2.1457672 *Math.pow(10, -5))+bit18*(1.0728836 *Math.pow(10, -5)));				

							
					/* setting every information extracted to the adequate attribute */
					setParity(the_parity); 
					setLatitude1(latitude1);
					
				}else{
						  throw new IllegalArgumentException("INVALID VALUE");
					  }
	}

}
