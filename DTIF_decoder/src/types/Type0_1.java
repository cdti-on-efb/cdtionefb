package types;
import genres.Data_word;


public class Type0_1 extends Data_word{
private transient int continuation_bit;
private transient String char1;
private transient String char2;
private transient String char3;
private String ID_part1;



public String getPart1() {
	return ID_part1;
}

public void setPart1(String part1) {
	this.ID_part1 = part1;
}

public String getChar1() {
    return char1;
}

public void setChar1(String char1) {
	this.char1 = char1;
}

public String getChar2() {
	return char2;
}

public void setChar2(String char2) {
	this.char2 = char2;
}

    public String getChar3() {
        return char3;
    }

    public void setChar3(String char3) {
        this.char3 = char3;
    }









	public Type0_1() {super();}

	public Type0_1(String value){
		super(value);
	}
	


//	public String getFlight_id() {
//		return flight_id;
//	}
//
//	public void setFlight_id(String flight_id) {
//		this.flight_id = flight_id;
//	}

	public int getContinuation_bit() {
		return continuation_bit;
	}

	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}
	
	
public void decryptValue() {
		
	// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
	continuation_bit = Character.getNumericValue(bits.charAt(1));
							
	setContinuation_bit(continuation_bit);
	
	//To check the data type termination
	
	//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
	int the_parity = Character.getNumericValue(bits.charAt(0));
	
	/* setting every information extracted to the adequate attribute */
	setParity(the_parity);
	
	if(continuation_bit == 0) {
            //Character 1,2&3 
            // extracting the 6 bits containing Character 1(bit number 13 to bit number 18 in arinc735b) in string format
            String the_char1 = bits.substring(14 ,20);
		
            if(the_char1.equals("000000")){
            	char1 = "";
            }
            else if(the_char1.equals("100000")){
            	char1 = "A";
            }else if(the_char1.equals("010000")){
            	char1 = "B";
            }else if(the_char1.equals("110000")){
            	char1 = "C";
            }else if(the_char1.equals("001000")){
            	char1 = "D";
            }else if(the_char1.equals("101000")){
            	char1 = "E";
            }else if(the_char1.equals("011000")){
            	char1 = "F";
            }else if(the_char1.equals("111000")){
            	char1 = "G";
            }else if(the_char1.equals("000100")){
            	char1 = "H";
            }else if(the_char1.equals("100100")){
            	char1 = "I";
            }else if(the_char1.equals("010100")){
            	char1 = "J";
            }else if(the_char1.equals("110100")){
            	char1 = "K";
            }else if(the_char1.equals("001100")){
            	char1 = "L";
            }else if(the_char1.equals("101100")){
            	char1 = "M";
            }else if(the_char1.equals("011100")){
            	char1 = "N";
            }else if(the_char1.equals("111100")){
            	char1 = "O";
            }else if(the_char1.equals("000010")){
            	char1 = "P";
            }else if(the_char1.equals("100010")){
            	char1 = "Q";
            }else if(the_char1.equals("010010")){
            	char1 = "R";
            }else if(the_char1.equals("110010")){
            	char1 = "S";
            }else if(the_char1.equals("001010")){
            	char1 = "T";
            }else if(the_char1.equals("101010")){
            	char1 = "U";
            }else if(the_char1.equals("011010")){
            	char1 = "V";
            }else if(the_char1.equals("111010")){
            	char1 = "W";
            }else if(the_char1.equals("000110")){
            	char1 = "X";
            }else if(the_char1.equals("100110")){
            	char1 = "Y";
            }else if(the_char1.equals("010110")){
            	char1 = "Z";
            }else if(the_char1.equals("000001")){
            	char1 = "SP";
            }else if(the_char1.equals("000011")){
            	char1 = "0";
            }else if(the_char1.equals("100011")){
            	char1 = "1";
            }else if(the_char1.equals("010011")){
            	char1 = "2";
            }else if(the_char1.equals("110011")){
            	char1 = "3";
            }else if(the_char1.equals("001011")){
            	char1 = "4";
            }else if(the_char1.equals("101011")){
            	char1 = "5";
            }else if(the_char1.equals("011011")){
            	char1 = "6";
            }else if(the_char1.equals("111011")){
            	char1 = "7";
            }else if(the_char1.equals("000111")){
            	char1 = "8";
            }else if(the_char1.equals("100111")){
            	char1 = "9";
            }
		
	
		
	// extracting the 6 bits containing Character 2(bit number 13 to bit number 18 in arinc735b) in string format
	String the_char2 = bits.substring(8 ,14);
				
	if(the_char2.equals("000000")){
    	char2 = "";
    }
    else if(the_char2.equals("100000")){
    	char2 = "A";
    }else if(the_char2.equals("010000")){
    	char2 = "B";
    }else if(the_char2.equals("110000")){
    	char2 = "C";
    }else if(the_char2.equals("001000")){
    	char2 = "D";
    }else if(the_char2.equals("101000")){
    	char2 = "E";
    }else if(the_char2.equals("011000")){
    	char2 = "F";
    }else if(the_char2.equals("111000")){
    	char2 = "G";
    }else if(the_char2.equals("000100")){
    	char2 = "H";
    }else if(the_char2.equals("100100")){
    	char2 = "I";
    }else if(the_char2.equals("010100")){
    	char2 = "J";
    }else if(the_char2.equals("110100")){
    	char2 = "K";
    }else if(the_char2.equals("001100")){
    	char2 = "L";
    }else if(the_char2.equals("101100")){
    	char2 = "M";
    }else if(the_char2.equals("011100")){
    	char2 = "N";
    }else if(the_char2.equals("111100")){
    	char2 = "O";
    }else if(the_char2.equals("000010")){
    	char2 = "P";
    }else if(the_char2.equals("100010")){
    	char2 = "Q";
    }else if(the_char2.equals("010010")){
    	char2 = "R";
    }else if(the_char2.equals("110010")){
    	char2 = "S";
    }else if(the_char2.equals("001010")){
    	char2 = "T";
    }else if(the_char2.equals("101010")){
    	char2 = "U";
    }else if(the_char2.equals("011010")){
    	char2 = "V";
    }else if(the_char2.equals("111010")){
    	char2 = "W";
    }else if(the_char2.equals("000110")){
    	char2 = "X";
    }else if(the_char2.equals("100110")){
    	char2 = "Y";
    }else if(the_char2.equals("010110")){
    	char2 = "Z";
    }else if(the_char2.equals("000001")){
    	char2 = "SP";
    }else if(the_char2.equals("000011")){
    	char2 = "0";
    }else if(the_char2.equals("100011")){
    	char2 = "1";
    }else if(the_char2.equals("010011")){
    	char2 = "2";
    }else if(the_char2.equals("110011")){
    	char2 = "3";
    }else if(the_char2.equals("001011")){
    	char2 = "4";
    }else if(the_char2.equals("101011")){
    	char2 = "5";
    }else if(the_char2.equals("011011")){
    	char2 = "6";
    }else if(the_char2.equals("111011")){
    	char2 = "7";
    }else if(the_char2.equals("000111")){
    	char2 = "8";
    }else if(the_char2.equals("100111")){
    	char2 = "9";
    }
		
	// extracting the 6 bits containing Character 3(bit number 25 to bit number 30 in arinc735b) in string format
	String the_char3 = bits.substring(2 ,8);
	
	if(the_char3.equals("000000")){
    	char3 = "";
    }
    else if(the_char3.equals("100000")){
    	char3 = "A";
    }else if(the_char3.equals("010000")){
    	char3 = "B";
    }else if(the_char3.equals("110000")){
    	char3 = "C";
    }else if(the_char3.equals("001000")){
    	char3 = "D";
    }else if(the_char3.equals("101000")){
    	char3 = "E";
    }else if(the_char3.equals("011000")){
    	char3 = "F";
    }else if(the_char3.equals("111000")){
    	char3 = "G";
    }else if(the_char3.equals("000100")){
    	char3 = "H";
    }else if(the_char3.equals("100100")){
    	char3 = "I";
    }else if(the_char3.equals("010100")){
    	char3 = "J";
    }else if(the_char3.equals("110100")){
    	char3 = "K";
    }else if(the_char3.equals("001100")){
    	char3 = "L";
    }else if(the_char3.equals("101100")){
    	char3 = "M";
    }else if(the_char3.equals("011100")){
    	char3 = "N";
    }else if(the_char3.equals("111100")){
    	char3 = "O";
    }else if(the_char3.equals("000010")){
    	char3 = "P";
    }else if(the_char3.equals("100010")){
    	char3 = "Q";
    }else if(the_char3.equals("010010")){
    	char3 = "R";
    }else if(the_char3.equals("110010")){
    	char3 = "S";
    }else if(the_char3.equals("001010")){
    	char3 = "T";
    }else if(the_char3.equals("101010")){
    	char3 = "U";
    }else if(the_char3.equals("011010")){
    	char3 = "V";
    }else if(the_char3.equals("111010")){
    	char3 = "W";
    }else if(the_char3.equals("000110")){
    	char3 = "X";
    }else if(the_char3.equals("100110")){
    	char3 = "Y";
    }else if(the_char3.equals("010110")){
    	char3 = "Z";
    }else if(the_char3.equals("000001")){
    	char3 = "SP";
    }else if(the_char3.equals("000011")){
    	char3 = "0";
    }else if(the_char3.equals("100011")){
    	char3 = "1";
    }else if(the_char3.equals("010011")){
    	char3 = "2";
    }else if(the_char3.equals("110011")){
    	char3 = "3";
    }else if(the_char3.equals("001011")){
    	char3 = "4";
    }else if(the_char3.equals("101011")){
    	char3 = "5";
    }else if(the_char3.equals("011011")){
    	char3 = "6";
    }else if(the_char3.equals("111011")){
    	char3 = "7";
    }else if(the_char3.equals("000111")){
    	char3 = "8";
    }else if(the_char3.equals("100111")){
    	char3 = "9";
    }


			
			setChar1(char1);
			setChar2(char2);
			setChar3(char3);
			
			StringBuilder sb = new StringBuilder(14);
			if(char1!="SP" && char1!=null)	sb.append(char1);
			if(char2!="SP" && char2!=null)	sb.append(char2);
			if(char3!="SP" && char3!=null)	sb.append(char3);
			ID_part1 = sb.toString();
			setPart1(ID_part1);
			
		
		
	}else{
                 throw new IllegalArgumentException("INVALID VALUE");
			  }
		 
			

			
	


}


}