package types;

import genres.Data_word;

public class Type2V0_3 extends Data_word{

	private transient int continuation_bit;
	private float longitude2;
	private String longitude_sign;
	private int relative_altitude;
	private String relative_altitude_sign;
	private String traffic_vertical_sense;
	private String relative_altitude_status;
	
	public Type2V0_3() {super();}

	public Type2V0_3(String value){
		super(value);
	}

	
	
	public float getLongitude2() {
		return longitude2;
	}

	public void setLongitude2(float longitude2) {
		this.longitude2 = longitude2;
	}

	public int getContinuation_bit() {
		return continuation_bit;
	}



	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}

	public String getLongitude_sign() {
		return longitude_sign;
	}



	public void setLongitude_sign(String longitude_sign) {
		this.longitude_sign = longitude_sign;
	}



	public int getRelative_altitude() {
		return relative_altitude;
	}



	public void setRelative_altitude(int relative_altitude) {
		this.relative_altitude = relative_altitude;
	}



	public String getRelative_altitude_sign() {
		return relative_altitude_sign;
	}



	public void setRelative_altitude_sign(String relative_altitude_sign) {
		this.relative_altitude_sign = relative_altitude_sign;
	}



	public String getTraffic_vertical_sense() {
		return traffic_vertical_sense;
	}



	public void setTraffic_vertical_sense(String traffic_vertical_sense) {
		this.traffic_vertical_sense = traffic_vertical_sense;
	}



	public String getRelative_altitude_status() {
		return relative_altitude_status;
	}



	public void setRelative_altitude_status(String relative_altitude_status) {
		this.relative_altitude_status = relative_altitude_status;
	}



	public void decryptValue() {
		// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
		int continuation_bit = Character.getNumericValue(bits.charAt(1));
						
		setContinuation_bit(continuation_bit);
						
			//To check the data type termination
			if(continuation_bit == 1) {
					
					//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
					int the_parity = Character.getNumericValue(bits.charAt(0));
					
					// extracting the 8 bits containing Longitude(bit number 09 to bit number 16 in arinc735b) in string format
					int bit9 = Character.getNumericValue(bits.charAt(23));
					int bit10 = Character.getNumericValue(bits.charAt(22));
					int bit11 = Character.getNumericValue(bits.charAt(21));
					int bit12 = Character.getNumericValue(bits.charAt(20));
					int bit13 = Character.getNumericValue(bits.charAt(19));
					int bit14 = Character.getNumericValue(bits.charAt(18));
					int bit15 = Character.getNumericValue(bits.charAt(17));
					int bit16 = Character.getNumericValue(bits.charAt(16));
					
					longitude2 = (float) (bit9*0.703125 + bit10*1.40625 + bit11*2.8125 +bit12*5.625 + bit13*11.25 + bit14*22.5 +bit15*45 +bit16*90) ;
					
					
					//extracting longitude sign bit number 17 in arinc735b (equivalent to bit number 11 in the dtif data provided) and converting the char extracted to int
					int the_longitude_sign = Character.getNumericValue(bits.charAt(15));
					
					//To convert it into Understandable Meaning
					if(the_longitude_sign == 0) {
						longitude_sign = "+ve(East)";
					}else if(the_longitude_sign == 1) {
						longitude_sign= "-ve(West)";
					}else {
						longitude_sign = "INVALID VALUE";
					}
					
					// extracting the 9 bits containing Relative Altitude(bit number 18 to bit number 26 in arinc735b) in string format
					String the_relative_altitude = bits.substring(6, 15);
					
					// converting the string extracted containing the 8 bits to decimal value using the function parseInt already existing in java
					relative_altitude = Integer.parseInt(the_relative_altitude, 2);
					
					//extracting Relative altitude sign bit number 27 in arinc735b (equivalent to bit number 19 in the dtif data provided) and converting the char extracted to int
					int the_relative_altitude_sign = Character.getNumericValue(bits.charAt(5));
					
					//To convert it into Understandable Meaning
					if(the_relative_altitude_sign == 0) {
						relative_altitude_sign = "+ve";
					}else if(the_relative_altitude_sign == 1) {
						relative_altitude_sign = "-ve";
					}else {
						relative_altitude_sign = "INVALID VALUE";
					}
					// extracting the 2 bits containing Traffic vertical Sense(bit number 28 to bit number 29 in arinc735b) in string format
					String traffic_vertical = bits.substring(3, 5);
					
					// converting the string extracted containing the 2 bits to decimal value using the function parseInt already existing in java
					int the_traffic_vertical_sense = Integer.parseInt(traffic_vertical, 2);
					
					//To determine the Traffic Vert Sense
					traffic_vertical_sense = "";
					if(the_traffic_vertical_sense  == 0){
						traffic_vertical_sense = "No Vertical rate/Level Flight";
					}else if(the_traffic_vertical_sense  == 1) {
						traffic_vertical_sense = "Climbing";
					}else if(the_traffic_vertical_sense  == 2) {
						traffic_vertical_sense = "Descending";
					}else if(the_traffic_vertical_sense == 3) {
						traffic_vertical_sense = "No Data";
					}else{
						traffic_vertical_sense= "INVALID VALUE";
					}
					
					//extracting Relative Altitude Status bit number 30 in arinc735b (equivalent to bit number 22 in the dtif data provided) and converting the char extracted to int
					int the_relative_altitude_status = Character.getNumericValue(bits.charAt(2));
					
					if(the_relative_altitude_status == 1) {
						relative_altitude_status = "Valid";
					}else if(the_relative_altitude_status == 0) {
						relative_altitude_status ="Invalid";
					}else {
						relative_altitude_status = "Not Defined";
					}
					
				
					
					/* setting every information extracted to the adequate attribute */
					setParity(the_parity); 
					setLongitude2(longitude2);
					setLongitude_sign(longitude_sign);
					setRelative_altitude(relative_altitude);
					setRelative_altitude_sign(relative_altitude_sign);
					setRelative_altitude_status(relative_altitude_status);
					setTraffic_vertical_sense(traffic_vertical_sense);
					
					
				}else{
					  throw new IllegalArgumentException("INVALID VALUE");
				  }

		}
}
