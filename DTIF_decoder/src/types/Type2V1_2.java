package types;

import genres.Data_word;

public class Type2V1_2 extends Data_word {

	private transient int continuation_bit;

	private  Float latitude2;
	private String latitude_sign;
	private Float longitude1;
	
	public Type2V1_2() {super();}

	public Type2V1_2(String value){
		super(value);
	} 
	public int getContinuation_bit() {
		return continuation_bit;
	}


	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}


	public String getLatitude_sign() {
		return latitude_sign;
	}


	public void setLatitude_sign(String latitude_sign) {
		this.latitude_sign = latitude_sign;
	}


    public Float getLatitude2() {
		return latitude2;
	}

	public void setLatitude2(Float latitude2) {
		this.latitude2 = latitude2;
	}



	public Float getLongitude1() {
		return longitude1;
	}

	public void setLongitude1(Float longitude1) {
		this.longitude1 = longitude1;
	}

	/**
     *
     */
    @Override
	public void decryptValue() {
		
				// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
				int continuation_bit = Character.getNumericValue(bits.charAt(1));
								
				setContinuation_bit(continuation_bit);
								
			
			// extracting the 5 bits containing Latitude(bit number 9 to bit number 13 in arinc735b) in string format
				int bit9 = Character.getNumericValue(bits.charAt(23));
				int bit10 = Character.getNumericValue(bits.charAt(22));
				int bit11 = Character.getNumericValue(bits.charAt(21));
				int bit12 = Character.getNumericValue(bits.charAt(20));
				int bit13 = Character.getNumericValue(bits.charAt(19));
				
				latitude2 = (float) (bit9*2.8125 + bit10*5.625 + bit11*11.25 +bit12*22.5 + bit13*45) ;
				
							
			//extracting latitude sign bit number 14 in arinc735b (equivalent to bit number 6 in the dtif data provided) and converting the char extracted to int
			int the_latitude_sign = Character.getNumericValue(bits.charAt(18));
							
			//To convert it into Understandable Meaning
			if(the_latitude_sign == 0) {
						latitude_sign = "+ve(North)";
				}else if(the_latitude_sign == 1) {
						latitude_sign= "-ve(South)";
				}else {
						latitude_sign = "INVALID VALUE";
					}
							
				// extracting the 16 bits containing Longitude(bit number 15 to bit number 30 in arinc735b) in string format
			int bit30 = Character.getNumericValue(bits.charAt(2));
			int bit29 = Character.getNumericValue(bits.charAt(3));
			int bit28 = Character.getNumericValue(bits.charAt(4));
			int bit27 = Character.getNumericValue(bits.charAt(5));
			int bit26 = Character.getNumericValue(bits.charAt(6));
			int bit25 = Character.getNumericValue(bits.charAt(7));
			int bit24 = Character.getNumericValue(bits.charAt(8));
			int bit23 = Character.getNumericValue(bits.charAt(9));
			int bit22 = Character.getNumericValue(bits.charAt(10));
			int bit21 = Character.getNumericValue(bits.charAt(11));
			int bit20 = Character.getNumericValue(bits.charAt(12));
			int bit19 = Character.getNumericValue(bits.charAt(13));
			int bit18 = Character.getNumericValue(bits.charAt(14));
			int bit17 = Character.getNumericValue(bits.charAt(15));
			int bit16 = Character.getNumericValue(bits.charAt(16));
			int bit15 = Character.getNumericValue(bits.charAt(17));

			
			longitude1 = (float) (bit30*0.351563 + bit29*0.175781 + bit28*0.0878906 +bit27*0.0439453 + bit26*0.0219726 +bit25*0.0109863 + bit24*0.00549316 + bit23*0.00274658 +bit22*0.00137329 +bit21*(6.8664551 *Math.pow(10, -4)) +bit20*(3.4332275 *Math.pow(10, -4)) +bit19*(1.7166138 *Math.pow(10, -4)) +bit18*(8.5830688 *Math.pow(10, -5))+bit17*(4.2915344 *Math.pow(10, -5))+bit16*(2.1457672 *Math.pow(10, -5))+bit15*(1.0728836 *Math.pow(10, -5)));				
							
				//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
			int the_parity = Character.getNumericValue(bits.charAt(0));
							
			/* setting every information extracted to the adequate attribute */
			setParity(the_parity); 
			setLatitude2(latitude2);
			setLatitude_sign(latitude_sign);
			setLongitude1(longitude1);	

	}

}
