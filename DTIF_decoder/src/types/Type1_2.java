package types;

import genres.Data_word;


public class Type1_2 extends Data_word{
 private transient int continuation_bit;
 private int relative_altitude;
 private String relative_altitude_sign;
 private int traffic_bearing;
 private String traffic_bearing_sign;
 private String bearing_status;

	
 public Type1_2() {super();}

	public Type1_2(String value){
		super(value);
	}
	


public int getContinuation_bit() {
	return continuation_bit;
}

public void setContinuation_bit(int continuation_bit) {
	this.continuation_bit = continuation_bit;
}


public int getRelative_altitude() {
	return relative_altitude;
}

public void setRelative_altitude(int relative_altitude) {
	this.relative_altitude = relative_altitude;
}

public String getRelative_altitude_sign() {
	return relative_altitude_sign;
}

public void setRelative_altitude_sign(String relative_altitude_sign) {
	this.relative_altitude_sign = relative_altitude_sign;
}

public int getTraffic_bearing() {
	return traffic_bearing;
}

public void setTraffic_bearing(int traffic_bearing) {
	this.traffic_bearing = traffic_bearing;
}

public String getTraffic_bearing_sign() {
	return traffic_bearing_sign;
}

public void setTraffic_bearing_sign(String traffic_bearing_sign) {
	this.traffic_bearing_sign = traffic_bearing_sign;
}

public String getBearing_status() {
	return bearing_status;
}

public void setBearing_status(String bearing_status) {
	this.bearing_status = bearing_status;
}

	public void decryptValue() {
		
		// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
		int continuation_bit = Character.getNumericValue(bits.charAt(1));
								
		setContinuation_bit(continuation_bit);
								
		if(continuation_bit == 1) {
			
			
			//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
			int the_parity = Character.getNumericValue(bits.charAt(0));
			
			// extracting the 9 bits containing Traffic Range(bit number 9 to bit number 17 in arinc735b) in string format
			String the_relative_altitude = bits.substring(15, 24);
			
			// converting the string extracted containing the 13 bits to decimal value using the function parseInt already existing in java
			relative_altitude = Integer.parseInt(the_relative_altitude, 2);
			
			//extracting Relative Altitude Sign bit number 18 in arinc735b (equivalent to bit number 10 in the dtif data provided) and converting the char extracted to int
			int the_relative_altitude_sign = Character.getNumericValue(bits.charAt(14));
			
			if(the_relative_altitude_sign == 0) {
				relative_altitude_sign = "+ve";
			}else if(the_relative_altitude_sign == 1) {
				relative_altitude_sign = "-ve";
			}else {
				relative_altitude_sign = "NOT DEFINED";
			}
			
			// extracting the 10 bits containing Traffic Bearing(bit number 19 to bit number 28 in arinc735b) in string format
			String the_traffic_bearing = bits.substring(4, 14);
						
			// converting the string extracted containing the 10 bits to decimal value using the function parseInt already existing in java
			traffic_bearing = Integer.parseInt(the_traffic_bearing, 2);
						
			//extracting Traffic Bearing Sign bit number 29 in arinc735b (equivalent to bit number 21 in the dtif data provided) and converting the char extracted to int
			int the_traffic_bearing_sign = Character.getNumericValue(bits.charAt(3));
			
			if(the_traffic_bearing_sign == 0) {
				traffic_bearing_sign = "+ve";
			}else if(the_traffic_bearing_sign == 1) {
				traffic_bearing_sign = "-ve";
			}else {
				traffic_bearing_sign = "NOT DEFINED";
			}
			
			
			//extracting Bearing Status bit number 30 in arinc735b (equivalent to bit number 22 in the dtif data provided) and converting the char extracted to int
			int the_bearing_status = Character.getNumericValue(bits.charAt(2));
			
			bearing_status = "";
			//TO display the Bearing Status
			
			if(the_bearing_status == 1) {
				bearing_status = "Valid";
			}else if(the_bearing_status == 0) {
				bearing_status ="Invalid";
			}else {
				bearing_status = "Not Defined";
			}
			
			/* setting every information extracted to the adequate attribute */
			setParity(the_parity);
			setRelative_altitude(relative_altitude);
			setRelative_altitude_sign(relative_altitude_sign);
			setTraffic_bearing(traffic_bearing);
			setTraffic_bearing_sign(traffic_bearing_sign);
			setBearing_status(bearing_status);		
			
		    }else{
			  throw new IllegalArgumentException("INVALID VALUE");
		      }
		
		}
}
