package types;

import genres.Data_word;


public class Type5 extends Data_word{

	private String position_offset;
	private transient int continuation_bit;
	private int width_code;
	private int length_code;
	private String len_wid_status;

	
	public Type5() {super();}

	public Type5(String value){
		super(value);
	}
	
	public String getPosition_offset() {
		return position_offset;
	}



	public void setPosition_offset(String position_offset) {
		this.position_offset = position_offset;
	}

	public int getContinuation_bit() {
		return continuation_bit;
	}



	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}



	public int getWidth_code() {
		return width_code;
	}



	public void setWidth_code(int width_code) {
		this.width_code = width_code;
	}



	public int getLength_code() {
		return length_code;
	}



	public void setLength_code(int length_code) {
		this.length_code = length_code;
	}



	public String getLen_wid_status() {
		return len_wid_status;
	}



	public void setLen_wid_status(String len_wid_status) {
		this.len_wid_status = len_wid_status;
	}



	public void decryptValue() {
	// extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
	int the_parity = Character.getNumericValue(bits.charAt(0));
				
	// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
	int continuation_bit = Character.getNumericValue(bits.charAt(1));
	
	//extracting length/width status bit number 18 in arinc735b (equivalent to bit number 10 in the dtif data provided) and converting the char extracted to int
	int the_len_wid_status = Character.getNumericValue(bits.charAt(14));
	
	 len_wid_status = "";
	//TO display the status
	if(the_len_wid_status == 0) {
		len_wid_status = "Invalid";
	}else if(the_len_wid_status == 1) {
		len_wid_status ="Valid";
	}else {
		len_wid_status = "NOT defined";
	}
	// extracting the 3 bits containing Length code(bit number 15 to bit number 17 in arinc735b) in string format
	String the_length_code = bits.substring(15, 18); 
	
	//converting the string extracted containing the 3 bits to decimal value using the function parseInt already existing in java
	length_code = Integer.parseInt(the_length_code, 2);
	
	//extracting width code bit number 14 in arinc735b (equivalent to bit number 6 in the dtif data provided) and converting the char extracted to int
	width_code = Character.getNumericValue(bits.charAt(18));
	
	//extracting Position Offset bit number 13 in arinc735b (equivalent to bit number 5 in the dtif data provided) and converting the char extracted to int
	int the_posititon_offset = Character.getNumericValue(bits.charAt(19));
	
	position_offset = "";
	//TO display the Position Offset
	if(the_posititon_offset == 0) {
		position_offset = "the position transmitted in the surface position message is not known to be referenced to the ADS-B Position Reference Point of the A/V. Thus, the aircraft depiction is typically extended beyond the length/width size.";
	}else if(the_posititon_offset == 1) {
		position_offset ="The position transmitted in the surface position message is known to be referenced to the ADS-B Position Reference Point of the A/V. Thus, the aircraft depiction typically matches the length/width size.";
	}else {
		position_offset = "NOT defined";
	}
	
	
	/* setting every information extracted to the adequate attribute */
	setContinuation_bit(continuation_bit);
	setParity(the_parity);
	setPosition_offset(position_offset);
	setWidth_code(width_code);
	setLength_code(length_code);
	setLen_wid_status(len_wid_status);
	
	
		}
}
