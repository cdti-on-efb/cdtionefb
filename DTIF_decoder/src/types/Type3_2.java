package types;

import genres.Data_word;



public class Type3_2 extends Data_word{
	
	private transient int continuation_bit;
	private int track_angle;
	private String track_angle_sign;
	private int closure_rate;
	private String closure_rate_sign;
	private String closure_rate_status;

	public Type3_2() {super();}

	public Type3_2(String value){
		super(value);
	}
	




	
	
	
	

	public String getClosure_rate_sign() {
		return closure_rate_sign;
	}

	public void setClosure_rate_sign(String closure_rate_sign) {
		this.closure_rate_sign = closure_rate_sign;
	}


	public String getClosure_rate_status() {
		return closure_rate_status;
	}

	public void setClosure_rate_status(String closure_rate_status) {
		this.closure_rate_status = closure_rate_status;
	}

	


	public int getContinuation_bit() {
		return continuation_bit;
	}




	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}





	public String getTrack_angle_sign() {
		return track_angle_sign;
	}

	public void setTrack_angle_sign(String track_angle_sign) {
		this.track_angle_sign = track_angle_sign;
	}

	



	


	



	public void setTrack_angle(int track_angle) {
		this.track_angle = track_angle;
	}




	public int getClosure_rate() {
		return closure_rate;
	}




	public void setClosure_rate(int closure_rate) {
		this.closure_rate = closure_rate;
	}






	public void decryptValue() {
		
		// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
		int continuation_bit = Character.getNumericValue(bits.charAt(1));
						
		setContinuation_bit(continuation_bit);
						
			//To check the data type termination
			if(continuation_bit == 1) {
				
			
				//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
				int the_parity = Character.getNumericValue(bits.charAt(0));
				
				// extracting the 7 bits containing Heading/Track Angle (bit number 9 to bit number 15 in arinc735b) in string format
				String the_track_angle = bits.substring(17, 24); 
				
				// converting the string extracted containing the 7 bits to decimal value using the function parseInt already existing in java
				track_angle = Integer.parseInt(the_track_angle, 2);
				
				//extracting Heading/Track Angle sign bit number 16 in arinc735b (equivalent to bit number 8 in the dtif data provided) and converting the char extracted to int
				int the_track_angle_bit = Character.getNumericValue(bits.charAt(16));
				
				//To convert it into sign
				track_angle_sign = " ";
					if(the_track_angle_bit == 0) {
						track_angle_sign = "+";
					}else if(the_track_angle_bit == 1) {
						track_angle_sign =  "-";
					}else{
						track_angle_sign = "INVALID VALUE";
					}
					
				// extracting the 12 bits containing Closure Rate (bit number 9 to bit number 15 in arinc735b) in string format
				String the_closure_rate = bits.substring(4, 16); 
				
				// converting the string extracted containing the 12 bits to decimal value using the function parseInt already existing in java
				closure_rate = Integer.parseInt(the_closure_rate, 2);
				
				//extracting closure rate sign bit number 29 in arinc735b (equivalent to bit number 21 in the dtif data provided) and converting the char extracted to int
				int the_closure_rate_sign_bit = Character.getNumericValue(bits.charAt(3));
				
				//To convert it into sign
				  closure_rate_sign = "";
				if(the_closure_rate_sign_bit == 0) {
					closure_rate_sign = "Departing from own ship";
				}else if(the_closure_rate_sign_bit == 1) {
					closure_rate_sign = "Closing on own Ship";
				}else {
					closure_rate_sign = "INVALID VALUE";
				}
				//extracting closure rate status bit number 30 in arinc735b (equivalent to bit number 22 in the dtif data provided) and converting the char extracted to int
				int the_closure_rate_status_bit = Character.getNumericValue(bits.charAt(2));
				
				 closure_rate_status = "";
				//TO display the status
				if(the_closure_rate_status_bit == 0) {
					closure_rate_status = "Invalid";
				}else if(the_closure_rate_status_bit == 1) {
					closure_rate_status ="Valid";
				}else {
					closure_rate_status = "Not Defined";
				}
				
				/* setting every information extracted to the adequate attribute */
				setParity(the_parity); 
				setTrack_angle(track_angle);
				setTrack_angle_sign(track_angle_sign);
				setClosure_rate(closure_rate);
				setClosure_rate_sign(closure_rate_sign);
				setClosure_rate_status(closure_rate_status);
				}else{
				  throw new IllegalArgumentException("INVALID VALUE");
			  }
		
		}

}
