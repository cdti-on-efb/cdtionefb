package types;

import genres.Data_word;

public class Type2V1_3 extends Data_word {


	private transient int continuation_bit;
	private float longitude2;
	private String longitude_sign;
	private int vertical_speed;
	private String vertical_speed_status;
	private String vertical_speed_sign;

	
	
	public Type2V1_3() {super();}

	public Type2V1_3(String value){
		super(value);
	}
	

	public int getContinuation_bit() {
		return continuation_bit;
	}


	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}


	
	public float getLongitude2() {
		return longitude2;
	}

	public void setLongitude2(float longitude2) {
		this.longitude2 = longitude2;
	}

	public String getLongitude_sign() {
		return longitude_sign;
	}


	public void setLongitude_sign(String longitude_sign) {
		this.longitude_sign = longitude_sign;
	}


	public int getVertical_speed() {
		return vertical_speed;
	}


	public void setVertical_speed(int vertical_speed) {
		this.vertical_speed = vertical_speed;
	}


	public String getVertical_speed_status() {
		return vertical_speed_status;
	}


	public void setVertical_speed_status(String vertical_speed_status) {
		this.vertical_speed_status = vertical_speed_status;
	}


	public String getVertical_speed_sign() {
		return vertical_speed_sign;
	}


	public void setVertical_speed_sign(String vertical_speed_sign) {
		this.vertical_speed_sign = vertical_speed_sign;
	}

    /**
     *
     */
    @Override
	public void decryptValue() {
		
				// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
				int continuation_bit = Character.getNumericValue(bits.charAt(1));
								
				setContinuation_bit(continuation_bit);
								
					if(continuation_bit == 1) {
						
					
						//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
						int the_parity = Character.getNumericValue(bits.charAt(0));
						
						// extracting the 8 bits containing Longitude(bit number 09 to bit number 16 in arinc735b) in string format
						int bit9 = Character.getNumericValue(bits.charAt(23));
						int bit10 = Character.getNumericValue(bits.charAt(22));
						int bit11 = Character.getNumericValue(bits.charAt(21));
						int bit12 = Character.getNumericValue(bits.charAt(20));
						int bit13 = Character.getNumericValue(bits.charAt(19));
						int bit14 = Character.getNumericValue(bits.charAt(18));
						int bit15 = Character.getNumericValue(bits.charAt(17));
						int bit16 = Character.getNumericValue(bits.charAt(16));
						
						longitude2 = (float) (bit9*0.703125 + bit10*1.40625 + bit11*2.8125 +bit12*5.625 + bit13*11.25 + bit14*22.5 +bit15*45 +bit16*90) ;
						
						//extracting longitude sign bit number 17 in arinc735b (equivalent to bit number 11 in the dtif data provided) and converting the char extracted to int
						int the_longitude_sign = Character.getNumericValue(bits.charAt(15));
						
						//To convert it into Understandable Meaning
						if(the_longitude_sign == 0) {
							longitude_sign = "+ve(East)";
						}else if(the_longitude_sign == 1) {
							longitude_sign= "-ve(West)";
						}else {
							longitude_sign = "INVALID VALUE";
						}
						// extracting the 11 bits containing Vertical Speed(bit number 18 to bit number 28 in arinc735b) in string format
						String the_vertical_speed = bits.substring(4, 15);
						
						// converting the string extracted containing the 8 bits to decimal value using the function parseInt already existing in java
						vertical_speed = Integer.parseInt(the_vertical_speed, 2);
						
						//extracting vertical speed sign bit number 29 in arinc735b (equivalent to bit number 21 in the dtif data provided) and converting the char extracted to int
						int the_vertical_speed_sign = Character.getNumericValue(bits.charAt(3));
						
						//To convert it into Understandable Meaning
						if(the_vertical_speed_sign == 0) {
							vertical_speed_sign = "+ve(Climbing)";
						}else if(the_vertical_speed_sign == 1) {
							vertical_speed_sign= "-ve(Descending)";
						}else {
							vertical_speed_sign = "INVALID VALUE";
						}		
						
						//extracting closure rate status bit number 30 in arinc735b (equivalent to bit number 22 in the dtif data provided) and converting the char extracted to int
						int the_vertical_speed_status = Character.getNumericValue(bits.charAt(2));
						
						//To convert it into Understandable Meaning
						if(the_vertical_speed_status == 1) {
							vertical_speed_status = "Valid";
						}else if(the_vertical_speed_status == 0) {
							vertical_speed_status ="Invalid";
						}else {
							vertical_speed_status = "Not Defined";
						}
						
						/* setting every information extracted to the adequate attribute */
						setParity(the_parity); 
						setLongitude2(longitude2);
						setLongitude_sign(longitude_sign);
						setVertical_speed(vertical_speed);
						setVertical_speed_sign(vertical_speed_sign);
						setVertical_speed_status(vertical_speed_status);
						
					}else{
						  throw new IllegalArgumentException("INVALID VALUE");
					  }
		
	
		
	}

}
