package types;

import genres.Data_word;


public class Type1_1 extends Data_word{
 private transient int continuation_bit;
 private String traffic_vert_sense;
 private String coarse_Fine_Range;
 private int traffic_range;
 private String traffic_range_invalidity;
 private String relative_altitude_status;

	
 public Type1_1() {super();}

	public Type1_1(String value){
		super(value);
	}
	

public int getContinuation_bit() {
	return continuation_bit;
}

public void setContinuation_bit(int continuation_bit) {
	this.continuation_bit = continuation_bit;
}

public String getTraffic_vert_sense() {
	return traffic_vert_sense;
}

public void setTraffic_vert_sense(String traffic_vert_sense) {
	this.traffic_vert_sense = traffic_vert_sense;
}


public String getCoarse_Fine_Range() {
	return coarse_Fine_Range;
}

public void setCoarse_Fine_Range(String coarse_Fine_Range) {
	this.coarse_Fine_Range = coarse_Fine_Range;
}

public int getTraffic_range() {
	return traffic_range;
}

public void setTraffic_range(int trafic_range) {
	this.traffic_range = trafic_range;
}


public String getTraffic_range_invalidity() {
	return traffic_range_invalidity;
}

public void setTraffic_range_invalidity(String traffic_range_invalidity) {
	this.traffic_range_invalidity = traffic_range_invalidity;
}



public String getRelative_altitude_status() {
	return relative_altitude_status;
}

public void setRelative_altitude_status(String relative_altitude_status) {
	this.relative_altitude_status = relative_altitude_status;
}



	public void decryptValue() {
		
		// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
		int continuation_bit = Character.getNumericValue(bits.charAt(1));
								
		setContinuation_bit(continuation_bit);
								
		//To check the data type termination
		if(continuation_bit == 0){
			
		
			//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
			int the_parity = Character.getNumericValue(bits.charAt(0));
			
			// extracting the 2 bits containing Traffic Vert Sense(bit number 13 to bit number 14 in arinc735b) in string format
			String the_traffic_vert_sense = bits.substring(18, 20);
			
			// converting the string extracted containing the 2 bits to decimal value using the function parseInt already existing in java
			int the_traffic_vertical_sense = Integer.parseInt(the_traffic_vert_sense, 2);
			
			//To determine the Traffic Vert Sense
			traffic_vert_sense = "";
			if(the_traffic_vertical_sense  == 0){
				traffic_vert_sense = "No Vertical rate/Level Flight";
			}else if(the_traffic_vertical_sense  == 1) {
				traffic_vert_sense = "Climbing";
			}else if(the_traffic_vertical_sense  == 2) {
				traffic_vert_sense = "Descending";
			}else if(the_traffic_vertical_sense == 3) {
				traffic_vert_sense = "No Data";
			}else{
				traffic_vert_sense= "INVALID VALUE";
			}
			
			//extracting Coarse/Fine Range bit number 15 in arinc735b (equivalent to bit number 7 in the dtif data provided) and converting the char extracted to int
			int the_coarse_fine_range = Character.getNumericValue(bits.charAt(17));
			
			
			//To convert into Alphabetical understanding
			if(the_coarse_fine_range == 0) {
				coarse_Fine_Range = "Coarse Setting";
				}else if(the_coarse_fine_range == 1) {
					coarse_Fine_Range = "Fine Setting";
				}else {
					coarse_Fine_Range= "INVALID VALUE";
				}
			// extracting the 13 bits containing Traffic Range(bit number 16 to bit number 28 in arinc735b) in string format
			String the_traffic_range = bits.substring(4, 17);
			
			// converting the string extracted containing the 13 bits to decimal value using the function parseInt already existing in java
			traffic_range = Integer.parseInt(the_traffic_range, 2);
			
			//extracting Traffic Range Invalidity bit number 29 in arinc735b (equivalent to bit number 21 in the dtif data provided) and converting the char extracted to int
			int the_traffic_range_invalidity= Character.getNumericValue(bits.charAt(3));
			
			traffic_range_invalidity = "";
			//TO display the Traffic Range Invalidity
			
			if(the_traffic_range_invalidity == 0) {
				traffic_range_invalidity = "Valid";
			}else if(the_traffic_range_invalidity == 1) {
				traffic_range_invalidity ="Invalid";
			}else {
				traffic_range_invalidity = "Not Defined";
			}
			
			//extracting Relative altitude Status bit number 15 in arinc735b (equivalent to bit number 7 in the dtif data provided) and converting the char extracted to int
			int the_relative_altitude_status = Character.getNumericValue(bits.charAt(2));
			
			relative_altitude_status = "";
			//TO display the Relative Altitude Status
			
			if(the_relative_altitude_status == 1) {
				relative_altitude_status = "Valid";
			}else if(the_relative_altitude_status == 0) {
				relative_altitude_status ="Invalid";
			}else {
				relative_altitude_status = "Not Defined";
			}
			/* setting every information extracted to the adequate attribute */
			setParity(the_parity);
			setTraffic_vert_sense(traffic_vert_sense);
			setCoarse_Fine_Range(coarse_Fine_Range);
			setTraffic_range(traffic_range);
			setTraffic_range_invalidity(traffic_range_invalidity);
			setRelative_altitude_status(relative_altitude_status);
			
		}else{
			  throw new IllegalArgumentException("INVALID VALUE");
		      }
		
		}
}
