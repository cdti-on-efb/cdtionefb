package types;

import genres.Data_word;



public class Type3_1 extends Data_word{

	private String mag_true_heading;
	private int ground_speed;
	private transient int continuation_bit;
	private String ground_speed_status;
	private String track_angle_status;
	
	public Type3_1() {super();}

	public Type3_1(String value){
		super(value);
	}
	




	
	
	
	
	public String getMag_true_heading() {
		return mag_true_heading;
	}

	public void setMag_true_heading(String mag_true_heading) {
		this.mag_true_heading = mag_true_heading;
	}



	
	public int getGround_speed() {
		return ground_speed;
	}




	public void setGround_speed(int ground_speed) {
		this.ground_speed = ground_speed;
	}


	public int getContinuation_bit() {
		return continuation_bit;
	}




	public void setContinuation_bit(int continuation_bit) {
		this.continuation_bit = continuation_bit;
	}





	
	public String getGround_speed_status() {
		return ground_speed_status;
	}

	public void setGround_speed_status(String ground_speed_status) {
		this.ground_speed_status = ground_speed_status;
	}





	


	public String getTrack_angle_status() {
		return track_angle_status;
	}

	public void setTrack_angle_status(String track_angle_status) {
		this.track_angle_status = track_angle_status;
	}

	public void decryptValue() {
		
		// extracting Continuation bit number 31 in arinc735b (equivalent to bit number 23 in the dtif data provided) 		
		int continuation_bit = Character.getNumericValue(bits.charAt(1));
						
		setContinuation_bit(continuation_bit);
						
			//To check the data type termination
			if(continuation_bit == 0) {
					
				
				
				//extracting parity bit number 32 in arinc735b (equivalent to bit number 24 in the dtif data provided) and converting the char extracted to int
				int the_parity = Character.getNumericValue(bits.charAt(0));
				 
				// extracting Magnetic/True Heading Indication bit number 15 in arinc735b (equivalent to bit number 18 in the dtif data provided) and converting the char extracted to int
				int the_mag_true_head_bits = Character.getNumericValue(bits.charAt(17));
				mag_true_heading = "";
				//To display result of Magnetic or true Heading
				if(the_mag_true_head_bits == 0) {
					 mag_true_heading = "True Heading";
				}else if(the_mag_true_head_bits == 1) {
					 mag_true_heading ="Magnetic heading";
				}else {
					 mag_true_heading = "INVALID VALUE";
				}
				
				// extracting the 12 bits containing Ground Speed(bit number 16 to bit number 27 in arinc735b) in string format
				String the_ground_speed = bits.substring(5, 17);
				
				// converting the string extracted containing the 12 bits to decimal value using the function parseInt already existing in java
				ground_speed = Integer.parseInt(the_ground_speed, 2);
				
				// extracting Ground speed status bit number 29 in arinc735b (equivalent to bit number 21 in the dtif data provided) and converting the char extracted to int
				int the_ground_speed_status_bit = Character.getNumericValue(bits.charAt(3));
				
				 ground_speed_status = "";
				//TO display the status
				if(the_ground_speed_status_bit == 0) {
					ground_speed_status = "Invalid";
				}else if(the_ground_speed_status_bit == 1) {
					ground_speed_status ="Valid";
				}else {
					ground_speed_status = "NOT defined";
				}
				
				// extracting Track angle status bit number 30 in arinc735b (equivalent to bit number 22 in the dtif data provided) and converting the char extracted to int
				int the_track_angle_status_bit = Character.getNumericValue(bits.charAt(2));
				
				track_angle_status = "";
				//TO display the status
				if(the_track_angle_status_bit == 0) {
					 track_angle_status = "Invalid";
				}else if(the_track_angle_status_bit == 1) {
					 track_angle_status ="Valid";
				}else {
					 track_angle_status = "Not Defined";
				}
				
				/* setting every information extracted to the adequate attribute */
				setParity(the_parity); 
				setMag_true_heading(mag_true_heading);
				setGround_speed(ground_speed);
				setGround_speed_status(ground_speed_status);
				setTrack_angle_status(track_angle_status);
				
			}else{
				  throw new IllegalArgumentException("INVALID VALUE");
			  }
			
		}

}
